 -- adder.vhd
 --
 -- The adders used for calculating PC-plus-4 and branch targets
 -- CprE 381
 --
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.mips32.all;

entity A_adder is
  port (src1    : in  m32_word;
        src2    : in  m32_word;
        result  : out m32_word);
end entity;

-- Behavior modeling of ADDER
architecture behavior of A_adder is
begin
  ADD : process (src1, src2)
    variable a : integer;
    variable b : integer;
    variable c : integer;
  begin
    -- Pre-calculate
    a := to_integer(signed(src1));
    b := to_integer(signed(src2));
    c := a + b;
    
    -- Convert integer to 32-bit signal
    result <= std_logic_vector(to_signed(c, result'length));
  end process;
end behavior;
architecture structure of A_adder is
  
  component A_FullAdder is
    port(i_OperandA : in std_logic;
       i_OperandB : in std_logic;
       i_Carry    : in std_logic;
       o_Sum      : out std_logic;
       o_Carry    : out std_logic);
  end component;
  
  signal temp_Carryin : std_logic_vector(32 downto 0);
  
  begin
    temp_Carryin(0) <= '0';
    
    G1: for i in 0 to 31 generate
      FullAdder_i: A_FullAdder
        port map(i_OperandA => src1(i),
                 i_OperandB => src2(i),
                 i_Carry => temp_Carryin(i),
                 o_Sum => result(i),
                 o_Carry => temp_Carryin(i+1));
      end generate;
      
    
end structure;
