library IEEE;
use IEEE.std_logic_1164.all;
use work.mips32.all;

entity AdderTB is  
  port(o_result : out m32_word);
       
end AdderTB;
     
  architecture structure of AdderTB is
    
    
    component adder is
      port (src1    : in  m32_word;
            src2    : in  m32_word;
            result  : out m32_word);            
    end component;
    
    signal i_src1, i_src2, t_output : m32_word;
    
    begin
      adder_1 : adder
        port map(src1   => i_src1,
                 src2   => i_src2,
                 result => t_output);
    process
      begin
        i_src1 <= x"F0F0F0F0";
        i_src2 <= x"0F0F0F0F";
        o_result <= t_output;
        wait for 100 ns;
        
        i_src2 <= x"F0F0F0F0";
        o_result <= t_output;
        wait for 100 ns;
        
        i_src1 <= x"0000FF00";
        i_src2 <= x"00000100";
        o_result <= t_output;
        wait for 100 ns;
        
    end process;
    
  end structure;