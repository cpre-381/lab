library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.mips32.all;

entity regTB is
  generic (N : integer := 32); --Register size
  
  port(o_reg_value : out m32_vector(N-1 downto 0));
end regTB;

architecture structure of regTB is
   component reg is
    generic(M : integer := 32);                   -- Size of the register
    port (D      : in  m32_vector(M-1 downto 0);  -- Data input
         Q      : out m32_vector(M-1 downto 0);  -- Data output
         WE     : in  m32_1bit;                  -- Write enableenable
         reset  : in  m32_1bit;                  -- The clock signal
         clock  : in  m32_1bit);                 -- The reset signal
  end component;
  
  signal t_WE, t_reset, t_clock : m32_1bit;
  signal t_input : m32_vector(N-1 downto 0);
  
  begin
    g_reg : reg
      generic map (M => N)
      port map(D => t_input,
               Q => o_reg_value,
               WE => t_WE,
               reset => t_reset,
               clock => t_clock);
               
    process
      begin
        t_WE <= '1';
        t_reset <= '0';
        t_input <= x"F0F0F0F0";
        wait for 150 ns;
        
        t_input <= x"0000FFFF";
        wait for 150 ns;
        
        t_WE <= '0';
        t_reset <= '1';
        wait for 150 ns;
        
      end process;
      
  end structure;
        
      