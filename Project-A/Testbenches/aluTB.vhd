library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.mips32.all;

entity aluTB is
  port(o_result : out m32_word;
       o_zero   : out m32_1bit);
end entity;

architecture structure of aluTB is
  component ALU is
    port (rdata1      : in  m32_word;
          rdata2      : in  m32_word;
          alu_code    : in  m32_4bits;
          result      : out m32_word;
          zero        : out m32_1bit);
  end component;
  
  signal t_data1, t_data2 : m32_word;
  signal t_alu_code : m32_4bits;
  
  begin
    g_ALU : ALU
      port map(rdata1   => t_data1,
               rdata2   => t_data2,
               alu_code => t_alu_code,
               result   => o_result,
               zero     => o_zero);
               
  process
    begin
      --Set the signals
      t_data1 <= x"00FF00FF";
      t_data2 <= x"00010001";
      
      --AND
      t_alu_code <= "0000";
      wait for 100 ns;
      
      --OR
      t_alu_code <= "0001";
      wait for 100 ns;
      
      --Add
      t_alu_code <= "0010";
      wait for 100 ns;
      
      --Subtract
      t_alu_code <= "0110";
      wait for 100 ns;
      
      --slt
      t_alu_code <= "0111";
      wait for 100 ns;
      
      --NOR
      t_alu_code <= "1100";
      wait for 100 ns;
      
    end process;
end structure;