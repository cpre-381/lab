library IEEE;
use IEEE.std_logic_1164.all;

entity memTestBench is
  port(o_q : out std_logic_vector(31 downto 0));
end memTestBench;

architecture structure of memTestBench is

  component mem is
    generic(depth_exp_of_2 : integer := 10;
            mif_filename   : string := "dmem.mif");
            
    port(address  : in std_logic_vector(depth_exp_of_2 - 1 downto 0) := (Others => '0');
         byteena  : in std_logic_vector(3 downto 0) := (others =>'1');
         clock    : in std_logic := '1';
         data     : in std_logic_vector(31 downto 0) := (others => '0');
         wren     : in std_logic := '0';
         q        : out std_logic_vector(31 downto 0));
  end component;
  
  
  signal t_address : std_logic_vector(11 downto 0);
  signal t_clock, t_wren : std_logic;
  signal t_data, t_data1 : std_logic_vector(31 downto 0);
  
  
  begin
    dmem : mem
      generic map(depth_exp_of_2 => 12)
      port map(address => t_address,
               byteena => "1111",
               clock   => t_clock,
               data    => t_data,
               wren    => t_wren,
               q       => t_data1);
               
    process
      begin
        o_q <= t_data1;
        
        
        t_wren <= '0';
        t_address <= x"000";               
        wait for 100 ns;
        
        t_wren <= '1';
        t_address <= x"100";
        t_data <= t_data1;
        wait for 100 ns;
        
        t_wren <= '0';
        t_address <= x"100";
        o_q <= t_data1;
        wait for 100 ns;
        
        t_wren <= '0';
        t_address <= x"001";
        wait for 100 ns;
        
        t_wren <= '1';
        t_address <= x"101";
        t_data <= t_data1;
        wait for 100 ns;
        
        t_wren <= '0';
        t_address <= x"101";
        o_q <= t_data1;
        wait for 100 ns;
        
        t_wren <= '0';
        t_address <= x"002";
        t_data <= t_data1;
        wait for 100 ns;
        
        
        t_wren <= '1';
        t_address <= x"102";
        t_data <= t_data1;
        wait for 100 ns;
        
        t_wren <= '0';
        t_address <= x"103";
        o_q <= t_data1;
        wait for 100 ns;
        
        t_wren <= '0';
        t_address <= x"003";
        t_data <= t_data1;
        wait for 100 ns;
        
        t_wren <= '1';
        t_address <= x"103";
        t_data <= t_data1;
        wait for 100 ns;
        
        t_wren <= '0';
        t_address <= x"103";
        o_q <= t_data1;
        wait for 100 ns;
        
        t_wren <= '0';
        t_address <= x"004";
        t_data <= t_data1;
        wait for 100 ns;
        
        t_wren <= '1';
        t_address <= x"104";
        t_data <= t_data1;
        wait for 100 ns;
        
        t_wren <= '0';
        t_address <= x"104";
        o_q <= t_data1;
        wait for 100 ns;
        
        t_wren <= '0';
        t_address <= x"005";
        t_data <= t_data1;
        wait for 100 ns;
        
        t_wren <= '1';
        t_address <= x"105";
        t_data <= t_data1;
        wait for 100 ns;
        
        t_wren <= '0';
        t_address <= x"105";
        o_q <= t_data1;
        wait for 100 ns;
        
        t_wren <= '0';
        t_address <= x"006";
        t_data <= t_data1;
        wait for 100 ns;
        
        t_wren <= '1';
        t_address <= x"106";
        t_data <= t_data1;
        wait for 100 ns;
        
        t_wren <= '0';
        t_address <= x"106";
        o_q <= t_data1;
        wait for 100 ns;
        
        t_wren <= '0';
        t_address <= x"007";
        t_data <= t_data1;
        wait for 100 ns;
        
        t_wren <= '1';
        t_address <= x"107";
        t_data <= t_data1;
        wait for 100 ns;
        
        t_wren <= '0';
        t_address <= x"107";
        o_q <= t_data1;
        wait for 100 ns;
        
        t_wren <= '0';
        t_address <= x"008";
        t_data <= t_data1;
        wait for 100 ns;
        
        t_wren <= '1';
        t_address <= x"108";
        t_data <= t_data1;
        wait for 100 ns;
        
        t_wren <= '0';
        t_address <= x"108";
        o_q <= t_data1;
        wait for 100 ns;
        
        t_wren <= '0';
        t_address <= x"009";
        t_data <= t_data1;
        wait for 100 ns;
        
        t_wren <= '1';
        t_address <= x"109";
        t_data <= t_data1;
        wait for 100 ns;
        
        t_wren <= '0';
        t_address <= x"109";
        o_q <= t_data1;
        wait for 100 ns;
    end process;
    
  end structure;
        
        