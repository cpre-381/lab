library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.mips32.all;

entity regfileTB is
  port(o_rdata1 : out m32_word;
       o_rdata2 : out m32_word);
end regfileTB;

architecture structure of regfileTB is
  
  component regfile is
    port(src1   : in  m32_5bits;
         src2   : in  m32_5bits;
         dst    : in  m32_5bits;
         wdata  : in  m32_word;
         rdata1 : out m32_word;
         rdata2 : out m32_word;
         WE     : in  m32_1bit;
         reset  : in  m32_1bit;
         clock  : in  m32_1bit);
  end component;
  
  signal t_src1, t_src2, t_dst : m32_5bits;
  signal t_wdata : m32_word;
  signal t_WE, t_reset, t_clock : m32_1bit;
  
  begin
    g_regFile : regfile
      port map(src1   => t_src1,
               src2   => t_src2,
               dst    => t_dst,
               wdata  => t_wdata,
               rdata1 => o_rdata1,
               rdata2 => o_rdata2,
               WE     => t_WE,
               reset  => t_reset,
               clock  => t_clock);
               
     process
      begin
        --First, reset everything
        t_reset <= '1';
        
        t_src1 <= "00001";
        t_src2 <= "00100";
        wait for 100 ns;
        
        --Now, try assigning values
        t_reset <= '0';
        t_WE <= '1';
        t_dst <= "00001";
        t_wdata <= x"00FF00FF";
        wait for 100 ns;    
        
        
        t_WE <= '1';
        t_dst <= "00100";
        t_wdata <= x"00FF00FF";
        wait for 100 ns;
        
        t_WE <= '0';
        t_wdata <= x"FFFFFFFF";
        wait for 100 ns;
    end process;
  end structure;      