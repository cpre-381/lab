vcom -reportprogress 300 -work work {Project-A/mips32.vhd}
vcom -reportprogress 300 -work work {Project-A/resources/register_array_type.vhd}

vcom -reportprogress 300 -work work {Project-A/resources/and2.vhd}
vcom -reportprogress 300 -work work {Project-A/resources/inv.vhd}
vcom -reportprogress 300 -work work {Project-A/resources/or2.vhd}
vcom -reportprogress 300 -work work {Project-A/resources/xor2.vhd}
vcom -reportprogress 300 -work work {Project-A/resources/dff.vhd}

vcom -reportprogress 300 -work work {Project-A/resources/mux32-1.vhd}
vcom -reportprogress 300 -work work {Project-A/resources/decoder5-32.vhd}
vcom -reportprogress 300 -work work {Project-A/extender.vhd}
vcom -reportprogress 300 -work work {Project-A/mux2_n.vhd}

vcom -reportprogress 300 -work work {Project-A/FullAdderUnit.vhd}
vcom -reportprogress 300 -work work {Project-A/adder.vhd}
vcom -reportprogress 300 -work work {Project-A/alu.vhd}
vcom -reportprogress 300 -work work {Project-A/alu_control.vhd}
vcom -reportprogress 300 -work work {Project-A/control.vhd}

vcom -reportprogress 300 -work work {Project-A/mem.vhd}
vcom -reportprogress 300 -work work {Project-A/reg.vhd}
vcom -reportprogress 300 -work work {Project-A/regfile.vhd}

vcom -reportprogress 300 -work work {Project-A/cpu.vhd}
vcom -reportprogress 300 -work work {Project-A/tb_cpu.vhd}