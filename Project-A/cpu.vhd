-- cpu.vhd: Single-cycle implementation of MIPS32 for CprE 381, fall 2013,
-- Iowa State University
--
-- Zhao Zhang, fall 2013

-- The CPU entity. It connects to 1) an instruction memory, 2) a data memory, and 
-- 3) an external clock source.
--
-- Note: This is a partical sample
--

library IEEE;
use IEEE.std_logic_1164.all;
use work.mips32.all;

entity A_cpu is
  port (imem_addr   : out m32_word;     -- Instruction memory address
        inst        : in  m32_word;     -- Instruction
        dmem_addr   : out m32_word;     -- Data memory address
        dmem_read   : out m32_1bit;     -- Data memory read?
        dmem_write  : out m32_1bit;     -- Data memory write?
        dmem_wmask  : out m32_4bits;    -- Data memory write mask
        dmem_rdata  : in  m32_word;     -- Data memory read data
        dmem_wdata  : out m32_word;     -- Data memory write data
        reset       : in  m32_1bit;     -- Reset signal
        clock       : in  m32_1bit);    -- System clock
end A_cpu;

-- This architecture of CPU must be dominantly structural, with no bahavior 
-- modeling, and only data flow statements to copy/split/merge signals or 
-- with a single level of basic logic gates.
architecture structure of A_cpu is
  component A_control is
      port (op_code     : in  m32_6bits;
            reg_dst     : out m32_1bit;
            jump        : out m32_1bit;
            branch_eq   : out m32_1bit;
            branch_ne   : out m32_1bit;
            mem_read    : out m32_1bit;
            mem_to_reg  : out m32_1bit;        
            alu_op      : out m32_2bits;
            mem_write   : out m32_1bit;
            alu_src2    : out m32_1bit;
            reg_write   : out m32_1bit;
            j_to_reg    : out m32_1bit);
  end component;

  -- The register file
  component A_regfile is
     port(src1      : in  m32_5bits;
          src2      : in  m32_5bits;
          dst       : in  m32_5bits;
          wdata     : in  m32_word;
          rdata1    : out m32_word;
          rdata2    : out m32_word;
          WE        : in  m32_1bit;
          reset     : in  m32_1bit;
          clock     : in  m32_1bit);
  end component;
  
  --Register for PC
  component A_reg is
    generic (M : integer := 32);                  -- Size of the register
    port (D      : in  m32_vector(M-1 downto 0);  -- Data input
          Q      : out m32_vector(M-1 downto 0);  -- Data output
          WE     : in  m32_1bit;                  -- Write enableenable
          reset  : in  m32_1bit;                  -- The clock signal
          clock  : in  m32_1bit);                 -- The reset signal
  end component;
  
  -- 2-to-1 MUX
  component A_mux2to1 is
    generic (M    : integer := 32);    -- Number of bits in the inputs and output
    port (input0  : in m32_vector(M-1 downto 0);
          input1  : in m32_vector(M-1 downto 0);
          sel     : in m32_1bit;
          output  : out m32_vector(M-1 downto 0));
  end component;
  component A_adder is 
    port (src1    : in  m32_word;
          src2    : in  m32_word;
          result  : out m32_word);
  end component;
  component A_extender is
    generic(Y : integer := 8);
    port(input : in std_logic_vector(Y-1 downto 0);
         sign  : in std_logic;
         output: out std_logic_vector(31 downto 0));
  end component;
  component A_ALU_control is
    port (funct       : in  m32_6bits;
          alu_op      : in  m32_2bits;
          alu_code    : out m32_4bits;
          alu_src1    : out m32_1bit;
          jump_reg    : out m32_1bit);
  end component;
  component A_ALU is
    port (rdata1      : in  m32_word;
          rdata2      : in  m32_word;
          alu_code    : in  m32_4bits;
          result      : out m32_word;
          zero        : out m32_1bit);
  end component;
  --
  -- Signals in the CPU
  --
  
  -- PC-related signals
  signal PC_saved       : m32_word;
  signal PC_final       : m32_word;     -- PC for the current inst
  signal PC_next        : m32_word;
  signal PC_jump        : m32_word;
  signal PC_branch      : m32_word;
  signal PC_jump_mux    : m32_word;
  signal PC_branch_mux  : m32_word;
  -- More code here
  
  -- Instruction fields and derives
  signal opcode     : m32_6bits;    -- 6-bit opcode
  signal reg_rs     : m32_5bits;    -- 5-bit rs register
  signal reg_rt     : m32_5bits;    -- 5-bit rt register
  signal reg_rd     : m32_5bits;    -- 5-bit rd register
  signal shamt      : m32_5bits;
  signal funct      : m32_6bits;
  signal shamtExtend: m32_word;
  signal immediate  : m32_halfword;
  signal address    : m32_26bits;
  
  signal imm_extends: m32_word;
  signal imm_branch : m32_word;
  signal branch_sgnl: m32_1bit;
  signal reg_wdst   : m32_5bits;    -- 5-bit write register

  -- Control signals
  signal reg_dst    : m32_1bit;    -- Register destination
  signal reg_write  : m32_1bit;
  signal jump       : m32_1bit;    -- Should I jump
  signal j_to_reg   : m32_1bit;
  signal branch_eq  : m32_1bit;    -- should I branch
  signal branch_ne  : m32_1bit;    -- should I branch
  signal mem_to_reg : m32_1bit;
  signal alu_src1   : m32_1bit;
  signal alu_src2   : m32_1bit;
  signal ALU_Op     : m32_2bits;
  signal jump_to_reg   : m32_1bit;


  -- RegFile Outputs
  signal rdata1     : m32_word;
  signal rdata2     : m32_word;
  -- ALU
  signal alu_data1  : m32_word;
  signal alu_data2  : m32_word;
  signal alu_code   : m32_4bits;
  signal alu_result : m32_word;
  signal alu_zero   : m32_1bit;

  -- Other non-control signals connected to regfile
  -- More code here
  signal wdata      : m32_word;     -- Register write data
  signal JMUX_wdata : m32_word;     -- write data register after JREG MUX
  signal JMUX_reg_wdst : m32_5bits; -- write destination after Jaddr MUX

begin    
  -- Split the instructions into fields
  SPLIT : block
  begin
    opcode    <= inst(31 downto 26);
    reg_rs    <= inst(25 downto 21);
    reg_rt    <= inst(20 downto 16);
    reg_rd    <= inst(15 downto 11);
    shamt     <= inst(10 downto  6);
    funct     <= inst(5  downto  0);
    shamtExtend <= "000000000000000000000000000" & shamt;
    immediate <= inst(15 downto  0);
    address   <= inst(25 downto  0);
  end block;
  
  --SYNCRONIZE cpu with clock by making PC a register
  PC_reg : A_reg generic map (M => 32)
    port map (PC_final, PC_saved, '1', reset, clock);

--        dmem_wmask  : out m32_4bits;    -- Data memory write mask
--        dmem_wdata  : out m32_word;     -- Data memory write data
  imem_addr  <= PC_saved;
  dmem_addr  <= alu_result;
  dmem_wmask <= "1111";
  dmem_wdata <= rdata2;
  
  cpu_control : A_control
    port map (opcode, reg_dst, jump, branch_eq, branch_ne, dmem_read, mem_to_reg, ALU_Op, dmem_write, alu_src2, reg_write, j_to_reg);
  
  -- Choose which part of the instruction to use as the write register of the register file
  DST_MUX : A_mux2to1 generic map (M => 5)
    port map (reg_rt, reg_rd, reg_dst, reg_wdst);
  --for JAL, chose write data
  JREG_wdata_MUX: A_mux2to1 generic map(M => 32)
    port map (wdata, PC_Next, j_to_reg, JMUX_wdata);
  --for JAL, choose write destination
  Jaddr_wdata_MUX: A_mux2to1 generic map(M=> 5)
    port map (reg_wdst, "11111", j_to_reg, JMUX_reg_wdst);
  -- The register file
  REGFILE1 : entity work.A_regfile(structural)
    port map (reg_rs, reg_rt, JMUX_reg_wdst, JMUX_wdata, rdata1, rdata2, reg_write, reset, clock);

  --Sign Extend the Immediate Value (instruction [15..0])
  sExtend : A_extender generic map(Y => 16)
    port map (immediate, '1', imm_extends);
  
  --choose whether to use funct or Read data 1 as the 1st input of the ALU
  ALU_MUX1 : A_mux2to1 generic map(M => 32)
    port map (rdata1, shamtExtend, alu_src1, alu_data1);

  --choose whether to use the immediate value or Read data 2 as the 2nd input of the ALU
  ALU_MUX2 : A_mux2to1 generic map(M => 32)
    port map (rdata2, imm_extends, alu_src2, alu_data2);
    
  --choose which ALU operation to perform based on Controls ALUOp and Instruction[5..0]
  ALU_CTRL : A_ALU_control
    port map (funct, ALU_op, alu_code, alu_src1, jump_to_reg);
  
  --primary ALU for arithmatic
  prim_ALU : A_ALU
    port map (alu_data1, alu_data2, alu_code, alu_result, alu_zero);
        
  --Calculate address of the following instruction
  ADD_PC_NEXT : entity work.A_adder(structure)
    port map(PC_saved, x"00000004", PC_next);
    
  --Calculate address of Jump instruction type
  PC_jump <= PC_next(31 downto 28) & address & "00";
  
  --Calculate address of branch instruction
  imm_branch <= imm_extends(29 downto 0) & "00";
  --relate branch address to current instruction address
  ADD_PC_BRANCH : entity work.A_adder(structure)
    port map (PC_next, imm_branch, PC_branch);
  
  --And gate to choose when to branch
  branch_sgnl <= (branch_eq and alu_zero) or (branch_ne and (not alu_zero));
  
  --Choose between the following 3 options: 1) next instruction, 2) jump instruction 3) branch instruction
  BRANCH_MUX : A_mux2to1 generic map(M=> 32)
    port map (PC_next, PC_branch, branch_sgnl, PC_branch_mux);
  PC_mux_COMP : A_mux2to1 generic map(M=> 32)
    port map (PC_branch_mux, PC_jump, jump, PC_jump_mux);
  PC_JREG_mux : A_mux2to1 generic map(M=>32)
    port map (PC_jump_mux, rdata1, jump_to_reg, PC_final);
  --
  Data_to_write_MUX : A_mux2to1 generic map(M => 32)
    port map (alu_result,dmem_rdata, mem_to_reg, wdata);
end structure;

