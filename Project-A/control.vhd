-- control.vhd: CprE 381 F13 template file
-- 
-- The main control unit of MIPS
-- 
-- Note: This is a partial example, with nine control signals (no Jump
-- singal)

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.A_mips32.all;

entity A_control is
  port (op_code     : in  m32_6bits;
        reg_dst     : out m32_1bit;
        jump        : out m32_1bit;
        branch_eq   : out m32_1bit;
        branch_ne   : out m32_1bit;
        mem_read    : out m32_1bit;
        mem_to_reg  : out m32_1bit;        
        alu_op      : out m32_2bits;
        mem_write   : out m32_1bit;
        alu_src2    : out m32_1bit;
        reg_write   : out m32_1bit;
        j_to_reg    : out m32_1bit);
end A_control;
architecture rom of A_control is
  subtype code_t is m32_vector(11 downto 0);
  type rom_t is array (0 to 63) of code_t;
  -- The ROM content
  -- Format: reg_dst, alu_src, mem_to_reg, reg_write, mem_read, 
  -- mem_write, branch, alu_op(1), alu_op(0)
  signal rom : rom_t := (
    -- More code here
--    0  => "00100001100",--R-
--    2  => "00---000--1",--j
--    4  => "10010000--0",--beq
--    5  => "01010000--0",--bne
--    8  => "00110001100",--addi
--    35 => "00001101010",--lw
--    43 => "00001010--0",--sw
    0  => "001000011000",--R-
    2  => "000000000010",--j
    3  => "000000010011",--jal
    4  => "100100000000",--beq
    5  => "010100000000",--bne
    8  => "000010010000",--addi
    35 => "000011010100",--lw
    43 => "000010100000",--sw
    others=>"000000000000");

begin
  (branch_eq, branch_ne, alu_op(1), alu_op(0), alu_src2, mem_read, mem_write, reg_write, reg_dst, mem_to_reg, jump, j_to_reg) 
     <= rom(to_integer(unsigned(op_code)));
end rom;

