------------------------------------------------------------------------
-- Ryan Wade
-- CPRE 381
-- Iowa State University
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
USE ieee.numeric_std.ALL;
use work.A_register_array_type.all;

entity A_mux32_1 is
  port(i_A  : in registerArray;
       i_S  : in std_logic_vector(4 downto 0);
       o_F  : out std_logic_vector(31 downto 0));

end A_mux32_1;

architecture dataflow of A_mux32_1 is

begin

  o_F <= i_A(to_integer(unsigned(i_S)));
  
end dataflow;