------------------------------------------------------------------------
-- Ryan Wade
-- CPRE 381
-- Iowa State University
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

package A_register_array_type is
  type registerArray is array(0 to 31) of std_logic_vector(31 downto 0);
end package A_register_array_type;