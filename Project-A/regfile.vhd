-- regfile.vhd: Register file for the MIPS processor
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.mips32.all;
use work.A_register_array_type.all;

entity A_regfile is
  port(src1   : in  m32_5bits;
       src2   : in  m32_5bits;
       dst    : in  m32_5bits;
       wdata  : in  m32_word;
       rdata1 : out m32_word;
       rdata2 : out m32_word;
       WE     : in  m32_1bit;
       reset  : in  m32_1bit;
       clock  : in  m32_1bit);
end A_regfile;

architecture behavior of A_regfile is
  signal reg_array : m32_regval_array;
begin
  -- Register reset/write logic, guarded by clock rising edge
  P_WRITE : process (clock)
    variable r : integer;
  begin
    -- Write/reset logic
    if (rising_edge(clock)) then
      if (reset = '1') then
        for i in 0 to 31 loop
          reg_array(i) <= X"00000000";
        end loop;
      elsif (WE = '1') then
        r := to_integer(unsigned(dst));
        if not (r = 0) then         -- MIPS $0 stores 0
          reg_array(r) <= wdata;
        end if;
      end if;
    end if;
  end process;
  
  -- Register read logic 
  P_READ : process (reg_array, src1, src2)
    variable r1, r2 : integer;
  begin
    r1 := to_integer(unsigned(src1));
    r2 := to_integer(unsigned(src2));
    rdata1 <= reg_array(r1);
    rdata2 <= reg_array(r2);
  end process;
end behavior;

architecture structural of A_regfile is
  signal s_rw : m32_word;
  signal WEandRW : m32_word;
  signal reg_array : registerArray;
  component A_dff
    generic(N: integer := 32);
    port(i_CLK        : in std_logic;                          -- Clock input
         i_RST        : in std_logic;                          -- Reset input
         i_WE         : in std_logic;                          -- Write enable input
         i_D          : in std_logic_vector(N-1 downto 0);     -- wdata value input
         o_Q          : out std_logic_vector(N-1 downto 0));   -- wdata value output
  end component;

  component A_decoder5_32
    port(i_A  : in std_logic_vector(4 downto 0);
         o_B  : out std_logic_vector(31 downto 0));
  end component;

  component A_mux32_1
    port(i_A  : in registerArray;
         i_S  : in std_logic_vector(4 downto 0);
         o_F  : out std_logic_vector(31 downto 0));
  end component;
  begin
  
  -- Create a multiplexed input to the FF based on i_WE
  write_decoder: A_decoder5_32
    port map(dst, s_rw);
  read_mux_t: A_mux32_1
    port map(reg_array, src1, rdata1);
  read_mux_d: A_mux32_1
    port map(reg_array, src2, rdata2);
  reg_0: A_dff
    port map(i_CLK => clock,
         i_RST => reset,
         i_WE => '0',
         i_D => wdata,
         o_Q => reg_array(0));
  G1: for i in 1 to 31 generate
    WEandRW(i) <= WE and s_rw(i);
    reg_i: A_dff
    port map(i_CLK => clock,
         i_RST => reset,
         i_WE => WEandRW(i),
         i_D => wdata,
         o_Q => reg_array(i));   
  end generate;
end structural;