-- alu.vhd
-- 
-- The ALU unit
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.A_mips32.all;

entity A_ALU_control is
  port (funct       : in  m32_6bits;
        alu_op      : in  m32_2bits;
        alu_code    : out m32_4bits;
        alu_src1    : out m32_1bit;
        jump_reg    : out m32_1bit);
end entity;

architecture behavior of A_ALU_control is
  signal r : m32_word;    
begin
  P_Control : process (alu_op, funct)
  begin
    jump_reg <= '0';
    -- Select the result, convert to signal if necessary
    case (alu_op) is
      when "10" =>      -- R instruction
        case(funct) is
          when "100100" =>        --AND
            alu_code <= "0000";
            alu_src1 <= '0';
          when "100101" =>        --OR
            alu_code <= "0001";
            alu_src1 <= '0';
          when "100000" =>        --ADD
            alu_code <= "0010";
            alu_src1 <= '0';
          when "000000" =>        --SLL
            alu_code <= "0011";
			      alu_src1 <= '1';
          when "100010" =>        --SUB
            alu_code <= "0110";
            alu_src1 <= '0';            
          when "101010" =>        --SLT
            alu_code <= "0111";
            alu_src1 <= '0';
          when "100111" =>        --NOR
            alu_code <= "1100";
            alu_src1 <= '0';            
			    when "001000" =>        --JR
			      alu_code <= "1111";
            jump_reg <= '1';
            alu_src1 <= '0';
          when others =>
            alu_code <= "1111";
            alu_src1 <= '0';
        end case;        
      when "01" =>	-- BEQ, BNE
        alu_code <= "0110";
        alu_src1 <= '0';
      when "11" =>	
        alu_code <= "1111";
        alu_src1 <= '0';
      when "00" =>      -- Store/Load/addi
        alu_code <= "0010";
        alu_src1 <= '0';
      when others =>      -- Otherwise, make output to be 0
        alu_code <= "1111";
        alu_src1 <= '0';
    end case;
  end process;
end behavior;