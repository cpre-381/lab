------------------------------------------------------------------------
-- Joseph Zambreno
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- generate_example.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an example of using generic ports to
-- drive a "generate / for" block. 
--
--
-- NOTES:
-- 8/27/09 by JAZ::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity A_mux2to1 is
  generic(M : integer := 32);
  port(input0  : in std_logic_vector(M-1 downto 0);
       input1  : in std_logic_vector(M-1 downto 0);
       sel     : in std_logic;
       output  : out std_logic_vector(M-1 downto 0));

end A_mux2to1;

architecture structure of A_mux2to1 is

  component A_and2
    port(i_A  : in std_logic;
         i_B  : in std_logic;
         o_F  : out std_logic);
  end component;

  component A_or2
    port(i_A  : in std_logic;
         i_B  : in std_logic;
         o_F  : out std_logic);
  end component;

  component A_inv
    port(i_A  : in std_logic;
         o_F  : out std_logic);
  end component;

  -- Signals to store A*x, B*x
  signal tNotS : std_logic_vector(M-1 downto 0);
  signal tAndA : std_logic_vector(M-1 downto 0);
  signal tAndB : std_logic_vector(M-1 downto 0);

begin

  G1: for i in 0 to M-1 generate
    inv_i1: A_inv
      port MAP(sel      , tNotS(i)          );
    and_i1: A_and2
      port MAP(tNotS(i) , input0(i)  ,tAndA(i) );
    and_i2: A_and2
      port MAP(sel      , input1(i)  , tAndB(i));
    or_i1: A_or2
      port MAP(tAndA(i) , tAndB(i), output(i)  );
  end generate;
  
end structure;