onerror {resume}
vsim -gui work.tb_id_stage
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_id_stage/clock
add wave -noupdate /tb_id_stage/reset
add wave -noupdate -expand -group IF -group PC_Control -radix decimal /tb_id_stage/ifid.PC_4
add wave -noupdate -expand -group IF -group PC_Control -radix decimal /tb_id_stage/PC_info.PC_branch
add wave -noupdate -expand -group IF -group PC_Control -radix decimal /tb_id_stage/PC_info.PC_jump
add wave -noupdate -expand -group IF -group PC_Control -radix decimal /tb_id_stage/PC_info.PC_jr
add wave -noupdate -expand -group IF -group PC_Control /tb_id_stage/PC_info.PC_ctrl
add wave -noupdate -expand -group IF -radix decimal -childformat {{/tb_id_stage/imem_addr(31) -radix decimal} {/tb_id_stage/imem_addr(30) -radix decimal} {/tb_id_stage/imem_addr(29) -radix decimal} {/tb_id_stage/imem_addr(28) -radix decimal} {/tb_id_stage/imem_addr(27) -radix decimal} {/tb_id_stage/imem_addr(26) -radix decimal} {/tb_id_stage/imem_addr(25) -radix decimal} {/tb_id_stage/imem_addr(24) -radix decimal} {/tb_id_stage/imem_addr(23) -radix decimal} {/tb_id_stage/imem_addr(22) -radix decimal} {/tb_id_stage/imem_addr(21) -radix decimal} {/tb_id_stage/imem_addr(20) -radix decimal} {/tb_id_stage/imem_addr(19) -radix decimal} {/tb_id_stage/imem_addr(18) -radix decimal} {/tb_id_stage/imem_addr(17) -radix decimal} {/tb_id_stage/imem_addr(16) -radix decimal} {/tb_id_stage/imem_addr(15) -radix decimal} {/tb_id_stage/imem_addr(14) -radix decimal} {/tb_id_stage/imem_addr(13) -radix decimal} {/tb_id_stage/imem_addr(12) -radix decimal} {/tb_id_stage/imem_addr(11) -radix decimal} {/tb_id_stage/imem_addr(10) -radix decimal} {/tb_id_stage/imem_addr(9) -radix decimal} {/tb_id_stage/imem_addr(8) -radix decimal} {/tb_id_stage/imem_addr(7) -radix decimal} {/tb_id_stage/imem_addr(6) -radix decimal} {/tb_id_stage/imem_addr(5) -radix decimal} {/tb_id_stage/imem_addr(4) -radix decimal} {/tb_id_stage/imem_addr(3) -radix decimal} {/tb_id_stage/imem_addr(2) -radix decimal} {/tb_id_stage/imem_addr(1) -radix decimal} {/tb_id_stage/imem_addr(0) -radix decimal}} -subitemconfig {/tb_id_stage/imem_addr(31) {-height 15 -radix decimal} /tb_id_stage/imem_addr(30) {-height 15 -radix decimal} /tb_id_stage/imem_addr(29) {-height 15 -radix decimal} /tb_id_stage/imem_addr(28) {-height 15 -radix decimal} /tb_id_stage/imem_addr(27) {-height 15 -radix decimal} /tb_id_stage/imem_addr(26) {-height 15 -radix decimal} /tb_id_stage/imem_addr(25) {-height 15 -radix decimal} /tb_id_stage/imem_addr(24) {-height 15 -radix decimal} /tb_id_stage/imem_addr(23) {-height 15 -radix decimal} /tb_id_stage/imem_addr(22) {-height 15 -radix decimal} /tb_id_stage/imem_addr(21) {-height 15 -radix decimal} /tb_id_stage/imem_addr(20) {-height 15 -radix decimal} /tb_id_stage/imem_addr(19) {-height 15 -radix decimal} /tb_id_stage/imem_addr(18) {-height 15 -radix decimal} /tb_id_stage/imem_addr(17) {-height 15 -radix decimal} /tb_id_stage/imem_addr(16) {-height 15 -radix decimal} /tb_id_stage/imem_addr(15) {-height 15 -radix decimal} /tb_id_stage/imem_addr(14) {-height 15 -radix decimal} /tb_id_stage/imem_addr(13) {-height 15 -radix decimal} /tb_id_stage/imem_addr(12) {-height 15 -radix decimal} /tb_id_stage/imem_addr(11) {-height 15 -radix decimal} /tb_id_stage/imem_addr(10) {-height 15 -radix decimal} /tb_id_stage/imem_addr(9) {-height 15 -radix decimal} /tb_id_stage/imem_addr(8) {-height 15 -radix decimal} /tb_id_stage/imem_addr(7) {-height 15 -radix decimal} /tb_id_stage/imem_addr(6) {-height 15 -radix decimal} /tb_id_stage/imem_addr(5) {-height 15 -radix decimal} /tb_id_stage/imem_addr(4) {-height 15 -radix decimal} /tb_id_stage/imem_addr(3) {-height 15 -radix decimal} /tb_id_stage/imem_addr(2) {-height 15 -radix decimal} /tb_id_stage/imem_addr(1) {-height 15 -radix decimal} /tb_id_stage/imem_addr(0) {-height 15 -radix decimal}} /tb_id_stage/imem_addr
add wave -noupdate -expand -group IF -radix decimal /tb_id_stage/imem_inst
add wave -noupdate -expand -group IF /tb_id_stage/ifid.inst_str
add wave -noupdate -expand -group ID /tb_id_stage/idex.inst_str
add wave -noupdate -expand -group ID /tb_id_stage/nostall
add wave -noupdate -expand -group ID -group ID_Control -expand /tb_id_stage/idex
add wave -noupdate -group EX -group EX_OUT /tb_id_stage/EXMEM_fwd
add wave -noupdate -group MEM -group MEM_OUT -radix decimal -childformat {{/tb_id_stage/load_info.is_load -radix decimal} {/tb_id_stage/load_info.rt -radix decimal}} -expand -subitemconfig {/tb_id_stage/load_info.is_load {-radix decimal} /tb_id_stage/load_info.rt {-radix decimal}} /tb_id_stage/load_info
add wave -noupdate -group WB -radix decimal -childformat {{/tb_id_stage/wb.reg_write -radix decimal} {/tb_id_stage/wb.dst -radix decimal} {/tb_id_stage/wb.wdata -radix decimal}} -expand -subitemconfig {/tb_id_stage/wb.reg_write {-radix decimal} /tb_id_stage/wb.dst {-radix decimal} /tb_id_stage/wb.wdata {-radix decimal}} /tb_id_stage/wb
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {90726 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 214
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {100 ns}
update
run 2 us
update
