-- The IF stage of MIPS pipeline
--
-- Ryan Wade
--
-- CPRE 381
-- Spring 2015


library IEEE;
use IEEE.std_logic_1164.all;
use work.mips32.all;
use work.mips32pl.all;

entity if_stage is
  port (i                     : in  m32_PC_info;   -- Branch address
        o                     : out m32_IFID;      -- Pipeline output
        imem_addr             : out m32_word;      -- Instruction address to instruction memory
        imem_inst             : in  m32_word;      -- Instrunction from instruction memory
        nostall               : in  m32_1bit;      -- Stall signal inverted
        reset                 : in  m32_1bit;
        clock                 : in  m32_1bit);
end if_stage;

architecture structure of if_stage is
  -- The PC register
  component reg is
    port (D                   : in  m32_word;           -- Data input
          Q                   : out m32_word;           -- Data output
          WE                  : in  m32_1bit;           -- Write enable
          reset               : in  m32_1bit;           -- The reset/flush signal
          clock               : in  m32_1bit);          -- The clock signal
  end component;
  
  -- Instruction decoder, for debugging
  component instruction_decoder is
    port (i_instr_bit         : in  m32_word;
          c_clk               : in  m32_1bit;
          o_instr_str         : out string);
  end component;

  component mux4_1 is
    generic(N : Integer := 32);
    port(iSel : in m32_2bits;
         iA                   : in  std_logic_vector(N-1 downto 0);
         iB                   : in  std_logic_vector(N-1 downto 0);
         iC                   : in  std_logic_vector(N-1 downto 0);
         iD                   : in  std_logic_vector(N-1 downto 0);
         O                    : out std_logic_vector(N-1 downto 0));
  end component;
  
  component adder
    port (data1               : in  m32_word;
          data2               : in  m32_word;
          result              : out m32_word);
  end component;
  
  -- PC-related signals
  signal PC                   : m32_word;     -- Current PC
  signal PC_4                 : m32_word;     -- Next sequential PC
  signal PC_Next              : m32_word;     -- Next PC
  
begin
  REGISTERS : block
  begin
    -- Program counter
    PC_REG : reg
      port map (D             => PC_Next,
                Q             => PC,
                WE            => nostall,
                reset         => reset,
                clock         => clock);
  end block;

  PC_CONTROL : block
  begin
    -- MUX for resolving next PC
    PC_MUX : mux4_1
	  generic map(N => 32)
      port map (iSel          => i.PC_ctrl,
                iA            => PC_4,
                iB            => i.PC_branch,
                iC            => i.PC_jump,
                iD            => i.PC_jr,
                O             => PC_Next);

    -- The PC adder for PC+4
    PC_adder : adder
      port map (data1         => PC,
                data2         => x"00000004",
                result        => PC_4);
    -- Send out instruction address
    imem_addr           <= PC;
    o.PC_4              <= PC_4;
  end block;
  INSTRUCTION : block
  begin 
    -- Receive instruction from imem
    o.inst              <= imem_inst;

    -- Instruction decoder, for debugging
    -- Translate instruction binary back to a string in text format.
    -- Every pipeline register is augmented with the string.
    DECODER : instruction_decoder
      port map (i_instr_bit   => imem_inst,
                c_clk         => clock,
                o_instr_str   => o.inst_str);
  end block;
end structure;

