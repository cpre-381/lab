library IEEE;
use IEEE.std_logic_1164.all;
use work.mips32.all;
use work.mips32pl.all;

entity Schematic is
  
end Schematic;

architecture structure of Schematic is
  component if_stage is
    port(i                    : in  m32_PC_info;   -- Branch address
         o                    : out m32_IFID;      -- Pipeline output
         imem_addr            : out m32_word;      -- Instruction address to instruction memory
         imem_inst            : in  m32_word;      -- Instrunction from instruction memory
         nostall              : in  m32_1bit;      -- Stall signal inverted
         reset                : in  m32_1bit;
         clock                : in  m32_1bit);
  end component;

  component id_stage is
    port (i                   : in  m32_IFID;   -- Pipeline input
          wb                  : in  m32_WB;     -- Writeback input
          load_info           : in  m32_load_info;   -- Load information for data hazard detection
          EXMEM_fwd           : in  m32_fwd;
			 EX_fwd              : in  m32_fwd;
          PC_info             : out m32_PC_info;    -- which PC should be taken
          o                   : out m32_IDEX;   -- Pipeline output
          nostall             : out m32_1bit;    -- If there is a load-use stall
          reset               : in  m32_1bit;
          clock               : in  m32_1bit);
  end component;

  component ex_stage is
    port (i                   : in  m32_IDEX;   -- Pipeline input
          EXMEM_fwd           : in  m32_fwd;
          MEMWB_fwd           : in  m32_fwd;
          o                   : out m32_EXMEM;   -- Pipeline output
          load_info           : out m32_load_info;
			 EX_fwd              : out m32_fwd;
          nostall             : in  m32_1bit;    -- If there is a load-use stall
          reset               : in  m32_1bit;
          clock               : in  m32_1bit);
  end component;

  component mem_stage is
    port (i                     : in  m32_EXMEM;     -- Pipeline input
          EXMEM_fwd             : out m32_fwd;
          o                     : out m32_MEMWB;    -- Pipeline output
          dmem_ctrl             : out m32_dmem_ctrl;
          dmem_rdata            : in  m32_word;     -- Data memory read data
          reset                 : in  m32_1bit;
          clock                 : in  m32_1bit);
  end component;
  
  component wb_stage is
    port (i                     : in  m32_MEMWB;     -- Pipeline input
          o                     : out m32_WB;        -- Pipeline output
          reset                 : in  m32_1bit;
          clock                 : in  m32_1bit);
  end component;
  
  component mem is
    generic (
      depth_exp_of_2          : integer := 8;
      mif_filename            : string);
        port (address         : in  m32_vector(7 downto 0);
              byteena         : in  m32_vector(3 DOWNTO 0);
              clock           : in  m32_1bit;
              data            : in  m32_word;
              wren            : in  m32_1bit;
              q               : out m32_word);      
  end component;

  signal PC_info              : m32_PC_info;
  signal ifid                 : m32_IFID;
  signal idex                 : m32_IDEX;
  signal exmem                : m32_EXMEM;
  signal memwb                : m32_MEMWB;
  signal wb                   : m32_WB;
  signal dmem_ctrl            : m32_dmem_ctrl;
  signal dmem_rdata           : m32_word;
  signal load_info            : m32_load_info;
  signal EXMEM_fwd            : m32_fwd;
  signal MEMWB_fwd            : m32_fwd;
  signal EX_fwd               : m32_fwd;
  signal imem_addr, imem_inst : m32_word;
  signal nostall              : m32_1bit;
  signal reset                : m32_1bit;     -- Reset signal
  signal clock                : m32_1bit;     -- System clock

begin
  if_stage_1 : if_stage
    port map(i          => PC_info,
             o          => ifid,
             imem_addr  => imem_addr,
             imem_inst  => imem_inst,
             nostall    => nostall,
             reset      => reset,
             clock      => clock);

  id_stage_2 : id_stage
    port map(i          => ifid,
             wb         => wb,
             load_info  => load_info,
             EXMEM_fwd  => EXMEM_fwd,
				 EX_fwd     => EX_fwd,
             PC_info    => PC_info,
             o          => idex,
             nostall    => nostall,
             reset      => reset,
             clock      => clock);

  ex_stage_3 : ex_stage
    port map (i         => idex,
              EXMEM_fwd => EXMEM_fwd,
              MEMWB_fwd => MEMWB_fwd,
              o         => exmem,
              load_info => load_info,
				  EX_fwd    => EX_fwd,
              nostall   => nostall,
              reset     => reset,
              clock     => clock);
  mem_stage_4 : mem_stage
    port map (i         => exmem,
              EXMEM_fwd => EXMEM_fwd,
              o         => memwb,
              dmem_ctrl => dmem_ctrl,
              dmem_rdata=> dmem_rdata,
              reset     => reset,
              clock     => clock);
  wb_stage_5 : wb_stage
    port map (i         => memwb,
              o         => wb,
              reset     => reset,
              clock     => clock);

  MEMWB_fwd.reg_write <= wb.reg_write;
  MEMWB_fwd.dst       <= wb.dst;
  MEMWB_fwd.wdata     <= wb.wdata;

  INST_MEM : mem
    generic map (mif_filename => "Project-A/imem_bubble_sort.txt")
    port map (imem_addr(9 downto 2), "0000", clock, x"00000000", '0', imem_inst);
  
    DATA_MEM : mem
    generic map (mif_filename => "Project-A/dmem.txt")
    port map (dmem_ctrl.addr(9 downto 2), dmem_ctrl.wmask, clock, dmem_ctrl.wdata, dmem_ctrl.we, dmem_rdata);
end structure;