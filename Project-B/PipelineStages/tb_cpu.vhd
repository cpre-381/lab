-- tb_cpu.vhd: Test bench for CPU
--
-- CprE 381 sample code
--
-- Zhao Zhang, fall 2013
--
library IEEE;
use IEEE.std_logic_1164.all;
use work.mips32.all;
use work.mips32pl.all;
entity tb_cpu is
end tb_cpu;

architecture behavior of tb_cpu is
  --PROJECTA_COM
    component A_cpu is
      port (imem_addr   : out m32_word;     -- Instruction memory address
            inst        : in  m32_word;     -- Instruction
            dmem_addr   : out m32_word;     -- Data memory address
            dmem_read   : out m32_1bit;     -- Data memory read?
            dmem_write  : out m32_1bit;     -- Data memory write?
            dmem_wmask  : out m32_4bits;    -- Data memory write mask
            dmem_rdata  : in  m32_word;     -- Data memory read data
            dmem_wdata  : out m32_word;     -- Data memory write data
            reset       : in  m32_1bit;     -- Reset signal
            clock       : in  m32_1bit);    -- System clock
    end component;
  
    -- The memory component
    component A_mem is
      generic (
        depth_exp_of_2  : integer := 8;
        mif_filename    : string);
          port (
	        address   : in  m32_vector(7 downto 0);
            byteena   : in  m32_vector(3 DOWNTO 0);
            clock	    : in  m32_1bit;
            data      : in  m32_word;
            wren	     : in  m32_1bit;
            q	        : out m32_word);      
    end component;
    component instruction_decoder is
      port (i_instr_bit         : in  m32_word;
            c_clk               : in  m32_1bit;
            o_instr_str         : out string);
    end component;
  --PROJECTB_COM
    component if_stage is
      port(i                    : in  m32_PC_info;   -- Branch address
           o                    : out m32_IFID;      -- Pipeline output
           imem_addr            : out m32_word;      -- Instruction address to instruction memory
           imem_inst            : in  m32_word;      -- Instrunction from instruction memory
           nostall              : in  m32_1bit;      -- Stall signal inverted
           reset                : in  m32_1bit;
           clock                : in  m32_1bit);
    end component;

    component id_stage is
      port (i                   : in  m32_IFID;   -- Pipeline input
            wb                  : in  m32_WB;     -- Writeback input
            load_info           : in  m32_load_info;   -- Load information for data hazard detection
            EXMEM_fwd           : in  m32_fwd;
            EX_fwd              : in  m32_fwd;
            PC_info             : out m32_PC_info;    -- which PC should be taken
            o                   : out m32_IDEX;   -- Pipeline output
            nostall             : out m32_1bit;    -- If there is a load-use stall
            reset               : in  m32_1bit;
            clock               : in  m32_1bit);
    end component;

    component ex_stage is
      port (i                   : in  m32_IDEX;   -- Pipeline input
            EXMEM_fwd           : in  m32_fwd;
            MEMWB_fwd           : in  m32_fwd;
            o                   : out m32_EXMEM;   -- Pipeline output
            load_info           : out m32_load_info;
            EX_fwd              : out m32_fwd;
            nostall             : in  m32_1bit;    -- If there is a load-use stall
            reset               : in  m32_1bit;
            clock               : in  m32_1bit);
    end component;

    component mem_stage is
      port (i                     : in  m32_EXMEM;     -- Pipeline input
            EXMEM_fwd             : out m32_fwd;
            o                     : out m32_MEMWB;    -- Pipeline output
            dmem_ctrl             : out m32_dmem_ctrl;
            dmem_rdata            : in  m32_word;     -- Data memory read data
            reset                 : in  m32_1bit;
            clock                 : in  m32_1bit);
    end component;
  
    component wb_stage is
      port (i                     : in  m32_MEMWB;     -- Pipeline input
            o                     : out m32_WB;        -- Pipeline output
            reset                 : in  m32_1bit;
            clock                 : in  m32_1bit);
    end component;
  
    component mem is
      generic (
        depth_exp_of_2          : integer := 8;
        mif_filename            : string);
          port (address         : in  m32_vector(7 downto 0);
                byteena         : in  m32_vector(3 DOWNTO 0);
                clock           : in  m32_1bit;
                data            : in  m32_word;
                wren            : in  m32_1bit;
                q               : out m32_word);      
    end component;

  --PROJECTA_SIG
    signal A_imem_addr   : m32_word;     -- Instruction memory address
    signal A_inst        : m32_word;     -- Instruction
    signal A_dmem_addr   : m32_word;     -- Data memory address
    signal A_dmem_read   : m32_1bit;     -- Data memory read?
    signal A_dmem_write  : m32_1bit;     -- Data memory write?
    signal A_dmem_wmask  : m32_4bits;    -- Data memory write mask
    signal A_dmem_rdata  : m32_word;     -- Data memory read data
    signal A_dmem_wdata  : m32_word;     -- Data memory write data

  --PROJECTB_SIG
    signal PC_info              : m32_PC_info;
    signal ifid                 : m32_IFID;
    signal idex                 : m32_IDEX;
    signal exmem                : m32_EXMEM;
    signal memwb                : m32_MEMWB;
    signal wb                   : m32_WB;
    signal dmem_ctrl            : m32_dmem_ctrl;
    signal dmem_rdata           : m32_word;
    signal load_info            : m32_load_info;
    signal EXMEM_fwd            : m32_fwd;
    signal MEMWB_fwd            : m32_fwd;
	signal EX_fwd               : m32_fwd;
    signal imem_addr, imem_inst : m32_word;
    signal nostall              : m32_1bit;

  --COMMON_SIG
    signal reset       : m32_1bit;     -- Reset signal
    signal clock       : m32_1bit;     -- System clock
    signal A_inst_str  : m32_inst_str;
    signal B_inst_str  : m32_inst_str;

begin
  PROJECTA_CPU : block
  begin
    A_CPU1 : A_cpu
      port map (A_imem_addr, A_inst, A_dmem_addr, A_dmem_read, A_dmem_write, A_dmem_wmask, A_dmem_rdata, A_dmem_wdata, reset, clock);
     
    -- The instruction memory. Note that write mask is hard-wired to 0000,
    -- write-enable is '0', and write data is 0.
    A_INST_MEM : A_mem
      generic map (mif_filename => "Project-A/imem_bubble_sort.txt")
      port map (A_imem_addr(9 downto 2), "0000", clock, x"00000000", '0', A_inst);
      
    -- The data memory. Note that the write mask is hard wired to 1111, and
    -- both data and q are connected to dmem_data
    A_DATA_MEM : A_mem
      generic map (mif_filename => "Project-A/dmem.txt")
      port map (A_dmem_addr(9 downto 2), A_dmem_wmask, clock, A_dmem_wdata, A_dmem_write, A_dmem_rdata);
    
    A_DECODER : instruction_decoder
      port map (i_instr_bit   => A_inst,
                c_clk         => clock,
                o_instr_str   => A_inst_str); 
    -- Produce a clock signal whose rising edge happens at 
    -- the beginging of CCT. Report signals right before
    -- the end of a clock cycle
  end block;
  PROJECTB_CPU : block
  begin
    if_stage_1 : if_stage
      port map(i          => PC_info,
               o          => ifid,
               imem_addr  => imem_addr,
               imem_inst  => imem_inst,
               nostall    => nostall,
               reset      => reset,
               clock      => clock);

    id_stage_2 : id_stage
      port map(i          => ifid,
               wb         => wb,
               load_info  => load_info,
               EXMEM_fwd  => EXMEM_fwd,
               EX_fwd     => EX_fwd,
               PC_info    => PC_info,
               o          => idex,
               nostall    => nostall,
               reset      => reset,
               clock      => clock);

    ex_stage_3 : ex_stage
      port map (i         => idex,
                EXMEM_fwd => EXMEM_fwd,
                MEMWB_fwd => MEMWB_fwd,
                o         => exmem,
                load_info => load_info,
                EX_fwd    => EX_fwd,
                nostall   => nostall,
                reset     => reset,
                clock     => clock);
    
	mem_stage_4 : mem_stage
      port map (i         => exmem,
                EXMEM_fwd => EXMEM_fwd,
                o         => memwb,
                dmem_ctrl => dmem_ctrl,
                dmem_rdata=> dmem_rdata,
                reset     => reset,
                clock     => clock);
    wb_stage_5 : wb_stage
      port map (i         => memwb,
                o         => wb,
                reset     => reset,
                clock     => clock);

    MEMWB_fwd.reg_write <= wb.reg_write;
    MEMWB_fwd.dst       <= wb.dst;
    MEMWB_fwd.wdata     <= wb.wdata;

    INST_MEM : mem
      generic map (mif_filename => "Project-A/imem_bubble_sort.txt")
      port map (imem_addr(9 downto 2), "0000", clock, x"00000000", '0', imem_inst);
  
    DATA_MEM : mem
      generic map (mif_filename => "Project-A/dmem.txt")
      port map (dmem_ctrl.addr(9 downto 2), dmem_ctrl.wmask, clock, dmem_ctrl.wdata, dmem_ctrl.we, dmem_rdata);
    B_DECODER : instruction_decoder
      port map (i_instr_bit   => imem_inst,
                c_clk         => clock,
                o_instr_str   => B_inst_str); 
    -- Produce a clock signal whose rising edge happens at 
    -- the beginging of CCT. Report signals right before
    -- the end of a clock cycle
  end block;
  CLOCK_SIGNAL : process 
    variable cycle : integer := 0;
  begin
    -- High for half cycle
    clock <= '1';
    wait for HCT;

    -- Low for half cycle
    clock <= '0';
    wait for HCT*4/5;    

    -- Debug: Print all signal values right before the rising edge
    report integer'image(cycle) & " " & hex(A_imem_addr) & 
      " " & hex(A_inst) & " " & hex (A_dmem_addr) & " " & 
      hex(A_dmem_rdata) & " " & hex(A_dmem_wdata);
    cycle := cycle + 1;
    wait for HCT*1/5;
  end process;
  
  TEST : process
  begin
    -- Wait for a small delay so that signal changes happen right before 
    -- the clock rising edge
    wait for 0.1*CCT;
    
    -- Reset the processor
    reset <= '1';
    wait for CCT;
    reset <= '0';
    
    -- Run for five clock cycles
    wait for 220*CCT;
    
    -- Force the simulation to stop
    assert false report "Simulation ends" severity failure;
  end process;

end behavior;