-- The MEM stage
--
-- Ryan Wade
--
-- CPRE 381
-- Spring 2015

library IEEE;
use IEEE.std_logic_1164.all;
use work.mips32.all;
use work.mips32pl.all;

entity mem_stage is
  port (i                     : in  m32_EXMEM;     -- Pipeline input
        EXMEM_fwd             : out m32_fwd;
        o                     : out m32_MEMWB;    -- Pipeline output
        dmem_ctrl             : out m32_dmem_ctrl;
        dmem_rdata            : in  m32_word;     -- Data memory read data
        reset                 : in  m32_1bit;
        clock                 : in  m32_1bit);
end mem_stage;

architecture structure of mem_stage is
  -- The IFID register
  component EXMEM_reg is
    port (D                   : in  m32_EXMEM;        -- EXMEM register input
          Q                   : out m32_EXMEM;        -- EXMEM register output
          WE                  : in  m32_1bit;         -- Write enable
          reset               : in  m32_1bit;         -- Flush signal
          clock               : in  m32_1bit);
  end component;
  
  -- 32 bit 4-1 MUX
  component mux4_1 is
    generic(N : Integer := 32);
    port(iSel : in m32_2bits;
         iA                   : in  std_logic_vector(N-1 downto 0);
         iB                   : in  std_logic_vector(N-1 downto 0);
         iC                   : in  std_logic_vector(N-1 downto 0);
         iD                   : in  std_logic_vector(N-1 downto 0);
         O                    : out std_logic_vector(N-1 downto 0));
  end component;
  
  signal r                    : m32_EXMEM;
begin
  REGISTERS : block
  begin
    -- The pipeline register
    EXMEM_REG1 : EXMEM_reg
      port map (D             => i,
                Q             => r,
                WE            => '1',
                reset         => reset,
                clock         => clock);
  end block;

  FORWARDING :block
  begin
    EXMEM_fwd.reg_write  <= r.WB_ctrl.reg_write;
    EXMEM_fwd.dst        <= r.dst;
    EXMEM_fwd.wdata      <= r.alu_result;
  end block;
  
  DMEM : block
  begin
    DMEM_MASK_MUX : mux4_1
    generic map(N => 4)
    port map (iSel          => r.MEM_ctrl.mem_size,
              iA            => "0001",
              iB            => "0011",
              iC            => "1111",
              iD            => "0000",
              O             => dmem_ctrl.wmask);     -- Data memory write mask
    dmem_ctrl.addr       <= r.alu_result;
    dmem_ctrl.re         <= r.MEM_ctrl.mem_read;     -- Data memory read?
    dmem_ctrl.we         <= r.MEM_ctrl.mem_write;    -- Data memory write?
    dmem_ctrl.wdata      <= r.rdata2;                -- Data memory write data
    o.mem_result         <= dmem_rdata;              -- Data memory read  data
  end block;

  o.inst_str             <= r.inst_str;
  o.WB_ctrl              <= r.WB_ctrl;	  
  o.alu_result           <= r.alu_result;
  o.dst                  <= r.dst;
  
end structure;