library IEEE;
use IEEE.std_logic_1164.all;
use work.mips32.all;
use work.mips32pl.all;

entity tb_if_stage is
  
end tb_if_stage;

architecture structure of tb_if_stage is
  component if_stage is
    port(i         : in  m32_PC_info;   -- Branch address
         o         : out m32_IFID;      -- Pipeline output
         imem_addr : out m32_word;      -- Instruction address to instruction memory
         imem_inst : in  m32_word;      -- Instrunction from instruction memory
         nostall   : in  m32_1bit;      -- Stall signal inverted
         reset     : in  m32_1bit;
         clock     : in  m32_1bit);
  end component;
  
  component mem is
    generic (
      depth_exp_of_2  : integer := 8;
      mif_filename    : string);
        port (address   : in  m32_vector(7 downto 0);
              byteena   : in  m32_vector(3 DOWNTO 0);
              clock     : in  m32_1bit;
              data      : in  m32_word;
              wren      : in  m32_1bit;
              q         : out m32_word);      
  end component;

  signal t_i : m32_PC_info;
  signal t_o : m32_IFID;
  signal imem_addr, imem_inst : m32_word;
  signal nostall     : m32_1bit;
  signal reset       : m32_1bit;     -- Reset signal
  signal clock       : m32_1bit;     -- System clock

begin
  if_stage_1 : if_stage
    port map(i         => t_i,
             o         => t_o,
             imem_addr => imem_addr,
             imem_inst => imem_inst,
             nostall   => nostall,
             reset     => reset,
             clock     => clock);

  INST_MEM : mem
    generic map (mif_filename => "Project-A/imem_bubble_sort.txt")
    port map (imem_addr(9 downto 2), "0000", clock, x"00000000", '0', imem_inst);
  -- Produce a clock signal whose rising edge happens at 
  -- the beginging of CCT. Report signals right before
  -- the end of a clock cycle
  CLOCK_SIGNAL : process 
    variable cycle : integer := 0;
  begin
    -- High for half cycle
    clock <= '1';
    wait for HCT;

    -- Low for half cycle
    clock <= '0';
    wait for HCT*4/5;    

    cycle := cycle + 1;
    wait for HCT*1/5;
  end process;
  
  TEST : process
  begin
    -- Wait for a small delay so that signal changes happen right before 
    -- the clock rising edge
    wait for 0.1*CCT;
    
    -- Reset the processor
    reset <= '1';
    t_i.PC_branch <= x"00000000";
    t_i.PC_jump   <= x"00000004";
    t_i.PC_jr     <= x"00000008";
    t_i.PC_ctrl   <= "00";
    nostall     <= '1';
    wait for CCT;
    reset <= '0';
    
    wait for 3*CCT;
    t_i.PC_ctrl <= "01";
    wait for 1*CCT;
    t_i.PC_ctrl <= "00";
    wait for 2*CCT;
    t_i.PC_ctrl <= "10";
    wait for 1*CCT;
    t_i.PC_ctrl <= "00";
    wait for 2*CCT;
    t_i.PC_ctrl <= "11";
    wait for 1*CCT;
    t_i.PC_ctrl <= "00";
    wait for 2*CCT;
    nostall <= '0';
    wait for 2*CCT;
    nostall <= '1';
    wait for 4*CCT;
    
    -- Force the simulation to stop
    assert false report "Simulation ends" severity failure;
  end process;
end structure;