-- idex_reg.vhd: The IDIE register
--
-- Zhao Zhang, CprE 381, fall 2013
--

library IEEE;
use IEEE.std_logic_1164.all;
use work.mips32.all;
use work.mips32pl.all;

entity memwb_reg is
  port (D      : in  m32_MEMWB;         -- Input from ID stage
        Q      : out m32_MEMWB;         -- Output to EX stage
        WE     : in  m32_1bit;          -- Write enable
        reset  : in  m32_1bit;          -- The reset/flush signal
        clock  : in  m32_1bit);         -- The clock signal
end memwb_reg;

architecture behavior of memwb_reg is
begin
  REG : process (clock)
  begin
    if (rising_edge(clock)) then
      if (reset = '1') then             -- Reset controls to zero
        Q.WB_ctrl.reg_write   <= '0';
        Q.WB_ctrl.mem_to_reg  <= '0';
        Q.inst_str  <= NOP;
      elsif (WE = '1') then
        Q <= D;
      end if;
    end if;
  end process;
end behavior;
