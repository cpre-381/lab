-- The WB stage
--
-- Ryan Wade
--
-- CPRE 381
-- Spring 2015

library IEEE;
use IEEE.std_logic_1164.all;
use work.mips32.all;
use work.mips32pl.all;

entity wb_stage is
  port (i                     : in  m32_MEMWB;     -- Pipeline input
        o                     : out m32_WB;     -- Pipeline output
        reset                 : in  m32_1bit;
        clock                 : in  m32_1bit);
end wb_stage;

architecture structure of wb_stage is
  -- The IFID register
  component MEMWB_reg is
    port (D                   : in  m32_MEMWB;        -- MEMWB register input
          Q                   : out m32_MEMWB;        -- MEMWB register output
          WE                  : in  m32_1bit;         -- Write enable
          reset               : in  m32_1bit;         -- Flush signal
          clock               : in  m32_1bit);
  end component;
  -- 32 bit 2-1 MUX
  component mux2_1 is
    generic (N : Integer := 32);
	port (iSel                : in  m32_1bit;
          iA                  : in  std_logic_vector(N-1 downto 0);
          iB                  : in  std_logic_vector(N-1 downto 0);
          O                   : out std_logic_vector(N-1 downto 0));
  end component;
  
  signal r                    : m32_MEMWB;
  signal wdata                : m32_word;
begin
  REGISTERS : block
  begin
    -- The pipeline register
    MEMWB_REG1 : MEMWB_reg
      port map (D             => i,
                Q             => r,
                WE            => '1',
                reset         => reset,
                clock         => clock);
  end block;

  MEM_TO_REG : block
  begin
    WB_MUX : mux2_1
      generic map(N => 32)
      port map (iSel          => r.WB_ctrl.mem_to_reg,
                iA            => r.alu_result,
                iB            => r.mem_result,
                O             => wdata);
  end block;
  FORWARDING_AND_WB :block
  begin
    o.reg_write  <= r.WB_ctrl.reg_write;
	o.dst        <= r.dst;
	o.wdata      <= wdata;
  end block;  
end structure;