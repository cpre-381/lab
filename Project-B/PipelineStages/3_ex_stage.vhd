-- The EX stage
--
-- Ryan Wade
--
-- CPRE 381
-- Spring 2015

library IEEE;
use IEEE.std_logic_1164.all;
use work.mips32.all;
use work.mips32pl.all;

entity ex_stage is
  port (i                     : in  m32_IDEX;   -- Pipeline input
        EXMEM_fwd             : in  m32_fwd;
        MEMWB_fwd             : in  m32_fwd;
        o                     : out m32_EXMEM;   -- Pipeline output
		load_info             : out m32_load_info;
		EX_fwd                : out m32_fwd;
        nostall               : in  m32_1bit;    -- If there is a load-use stall
        reset                 : in  m32_1bit;
        clock                 : in  m32_1bit);
end ex_stage;

architecture structure of ex_stage is
  -- The IFID register
  component IDEX_reg is
    port (D                   : in  m32_IDEX;         -- IDEX register input
          Q                   : out m32_IDEX;         -- IDEX register output
          WE                  : in  m32_1bit;         -- Write enable
          reset               : in  m32_1bit;         -- Flush signal
          clock               : in  m32_1bit);
  end component;
  
  --Arithmetic Unit
  component ALU is
    port (data1               : in  m32_word;
          data2               : in  m32_word;
          alu_code            : in  m32_4bits;
          result              : out m32_word);
  end component;
  
   -- Data Forward Unit
  component dataforward_ex is
    port (EXMEM_RegWrite      : in  m32_1bit;
          EXMEM_Rd            : in  m32_5bits;
		  MEMWB_RegWrite      : in  m32_1bit;
          MEMWB_Rd            : in  m32_5bits;
          rs                  : in  m32_5bits;
          rt                  : in  m32_5bits;
          rs_src              : out m32_2bits;
          rt_src              : out m32_2bits);
  end component;

  -- 32 bit 2-1 MUX
  component mux2_1 is
    generic(N : Integer := 32);
    port(iSel : in m32_1bit;
         iA                   : in  std_logic_vector(N-1 downto 0);
         iB                   : in  std_logic_vector(N-1 downto 0);
         O                    : out std_logic_vector(N-1 downto 0));
  end component;
  
  -- 32 bit 4-1 MUX
  component mux4_1 is
    generic(N : Integer := 32);
    port(iSel : in m32_2bits;
         iA                   : in  std_logic_vector(N-1 downto 0);
         iB                   : in  std_logic_vector(N-1 downto 0);
         iC                   : in  std_logic_vector(N-1 downto 0);
         iD                   : in  std_logic_vector(N-1 downto 0);
         O                    : out std_logic_vector(N-1 downto 0));
  end component;
  
  signal r                    : m32_IDEX;
  signal r_flush              : m32_1bit;
  
  signal rs_ctrl              : m32_1bit;
  signal rt_ctrl              : m32_1bit;
  
  signal data1_ctrl           : m32_2bits;
  signal data2_ctrl           : m32_2bits;

  signal data1_fwd            : m32_word;
  signal data2_fwd            : m32_word;
  
  signal data1_alu            : m32_word;
  signal data2_alu            : m32_word;
  
  signal alu_result           : m32_word;
  
  signal EX_result            : m32_word;
  signal EX_dst               : m32_5bits;
begin
  REGISTERS : block
  begin
    -- The pipeline register
    r_flush <= NOT nostall or reset;
    IDEX_REG1 : IDEX_reg
      port map (D             => i,
                Q             => r,
                WE            => '1',
                reset         => r_flush,
                clock         => clock);
  end block;

  FORWARDING : block
  begin
    FWD_EX : dataforward_ex
      port map (EXMEM_RegWrite      => EXMEM_fwd.reg_write,
                EXMEM_Rd            => EXMEM_fwd.dst,
                MEMWB_RegWrite      => MEMWB_fwd.reg_write,
                MEMWB_Rd            => MEMWB_fwd.dst,
                rs                  => r.rs_addr,
                rt                  => r.rt_addr,
                rs_src              => data1_ctrl,
                rt_src              => data2_ctrl);
		  
    DATA1_MUX : mux4_1
      generic map(N => 32)
      port map (iSel          => data1_ctrl,
                iA            => r.rdata1,
                iB            => EXMEM_fwd.wdata,
                iC            => MEMWB_fwd.wdata,
                iD            => x"00000000",
                O             => data1_fwd);	
				
    DATA2_MUX : mux4_1
      generic map(N => 32)
      port map (iSel          => data2_ctrl,
                iA            => r.rdata2,
                iB            => EXMEM_fwd.wdata,
                iC            => MEMWB_fwd.wdata,
                iD            => x"00000000",
                O             => data2_fwd);
  end block;
  
  ALU_BLOCK : block
  begin
	ALU_SRC1_MUX : mux2_1
	  generic map(N => 32)
	  port map (iSel          => r.alu_src1,
                iA            => data1_fwd,
                iB            => r.shamt,
                O             => data1_alu);
				
    ALU_SRC2_MUX : mux4_1
	  generic map(N => 32)
	  port map (iSel          => r.EX_ctrl.alu_src2,
                iA            => data2_fwd,
                iB            => r.imm_ext,
                iC            => r.imm_uext,
                iD            => x"00000000",
                O             => data2_alu);

    ALU1 : alu
      port map (data1     => data1_alu,
                data2     => data2_alu,
                alu_code  => r.alu_code,
                result    => alu_result);
    ALU_RESULT_MUX : mux2_1
	  generic map(N => 32)
	  port map (iSel           => r.EX_ctrl.reg_dst(1),
                iA            => alu_result,
                iB            => r.PC_4,
                O             => EX_result);
	o.alu_result <= EX_result;
  end block;
  
  WRITE_DST_AND_LOAD_INFO : block
  begin
    DST_REG : mux4_1
      generic map(N => 5)
      port map (iSel          => r.EX_ctrl.reg_dst,
                iA            => r.rt_addr,
                iB            => r.rd_addr,
                iC            => "11111",
                iD            => "00000",
                O             => EX_dst);
  o.dst                 <= EX_dst;
  load_info.is_load     <= r.WB_ctrl.mem_to_reg;
  load_info.rt          <= r.rt_addr;
  end block;
  EX_fwd.reg_write      <= r.WB_ctrl.reg_write;
  EX_fwd.dst            <= EX_dst;
  EX_fwd.wdata          <= EX_result;
  o.inst_str            <= r.inst_str;
  o.MEM_ctrl            <= r.MEM_ctrl;
  o.WB_ctrl             <= r.WB_ctrl;
  o.rdata2              <= data2_alu;
end structure;