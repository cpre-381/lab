-- The ID stage
--
-- Ryan Wade
--
-- CPRE 381
-- Spring 2015

library IEEE;
use IEEE.std_logic_1164.all;
use work.mips32.all;
use work.mips32pl.all;

entity id_stage is
  port (i                     : in  m32_IFID;   -- Pipeline input
        wb                    : in  m32_WB;     -- Writeback input
        load_info             : in  m32_load_info;   -- Load information for data hazard detection
        EXMEM_fwd             : in  m32_fwd;
		EX_fwd                : in  m32_fwd;
        PC_info               : out m32_PC_info;    -- which PC should be taken
        o                     : out m32_IDEX;   -- Pipeline output
        nostall               : out m32_1bit;    -- If there is a load-use stall
        reset                 : in  m32_1bit;
        clock                 : in  m32_1bit);
end id_stage;
        
architecture structure of id_stage is
  -- The IFID register
  component IFID_reg is
    port (D                   : in  m32_IFID;         -- IFID register input
          Q                   : out m32_IFID;         -- IFID register output
          WE                  : in  m32_1bit;         -- Write enable
          reset               : in  m32_1bit;         -- Flush signal
          clock               : in  m32_1bit);
  end component;

  -- The main control unit  
  component control is
    port (op_code             : in  m32_6bits;
          ID_ctrl             : out m32_ID_ctrl;
          EX_ctrl             : out m32_EX_ctrl;
          MEM_ctrl            : out m32_MEM_ctrl;
          WB_ctrl             : out m32_WB_ctrl);
  end component;
 
  -- The ALU control unit
  component alu_ctrl is
    port (funct               : in  m32_6bits;
          alu_op              : in  m32_3bits;
          alu_code            : out m32_4bits;
          alu_src1            : out m32_1bit;
          jump_reg            : out m32_1bit);
  end component;

 
  -- The register file
  component regfile is
    port (src1                : in  m32_5bits;
          src2                : in  m32_5bits;
          dst                 : in  m32_5bits;
          wdata               : in  m32_word;
          rdata1              : out m32_word;
          rdata2              : out m32_word;
          WE                  : in  m32_1bit;
          reset               : in  m32_1bit);
  end component;
  
  -- Data Hazard unit
  component hazard_unit is
    port (load_info           : in  m32_load_info;  --load rt
          rs                  : in  m32_5bits;
          rt                  : in  m32_5bits;
          nostall             : out m32_1bit);
  end component;
  
  -- Data Forward Unit
  component dataforward_id is
    port (EXMEM_RegWrite      : in m32_1bit;
          EXMEM_Rd            : in m32_5bits;
	       EX_RegWrite         : in  m32_1bit;
          EX_Rd               : in  m32_5bits;
          rs                  : in m32_5bits;
          rt                  : in m32_5bits;
          rs_src              : out m32_2bits;
          rt_src              : out m32_2bits);
  end component;
  
  -- Branch Logic Unit
  component blu is
    port (rdata1              : in  m32_word;
          rdata2              : in  m32_word;
          beq                 : in  m32_1bit;
          bne                 : in  m32_1bit;
          branchTaken         : out m32_1bit);
  end component;
  
  -- Data Extender Unit       
  component extender
    generic (Y                : integer := 8);
    port (input               : in  std_logic_vector(Y-1 downto 0);
          sign                : in  m32_1bit;
          output              : out m32_word);
  end component;

  -- Adder
  component adder
    port (data1               : in  m32_word;
          data2               : in  m32_word;
          result              : out m32_word);
  end component;

  -- 32 bit 4-1 MUX
  component mux4_1 is
    generic(N : Integer := 32);
    port(iSel : in m32_2bits;
         iA                   : in  std_logic_vector(N-1 downto 0);
         iB                   : in  std_logic_vector(N-1 downto 0);
		 iC                   : in  std_logic_vector(N-1 downto 0);
         iD                   : in  std_logic_vector(N-1 downto 0);
         O                    : out std_logic_vector(N-1 downto 0));
  end component;
  
  -- Signals
  signal r                    : m32_IFID;   -- The IFID register
  signal r_flush              : m32_1bit;   -- IFID register reset/flush
  signal clock_inv            : m32_1bit;   -- inverted clock signal

  signal ID_ctrl              : m32_ID_ctrl;
  signal jr                   : m32_1bit;
  
  signal rs_ctrl              : m32_2bits;
  signal rt_ctrl              : m32_2bits;
  
  signal brn_data1            : m32_word;
  signal brn_data2            : m32_word;
  
  signal rdata1               : m32_word;
  signal rdata2               : m32_word;
  
  signal s_nostall              : m32_1bit;
  
  signal inst                 : m32_inst;
  
  signal branch_result        : m32_1bit;
  
begin
  INSTRUCTION : block
  begin
    inst.opcode         <= r.inst(31 downto 26);
    inst.rs             <= r.inst(25 downto 21);
    inst.rt             <= r.inst(20 downto 16);
    inst.rd             <= r.inst(15 downto 11);
    inst.funct          <= r.inst(5  downto  0);
    inst.shamt          <= "000000000000000000000000000" & r.inst(10 downto  6);
    inst.imm            <= r.inst(15 downto  0);
    inst.addr           <= r.inst(25 downto  0);

    -- The sign extender for immediate
    sExtend : extender generic map (Y => 16)
      port map (input         => inst.imm,
                sign          => '1',
                output        => inst.imm_ext);
	uExtend : extender generic map (Y => 16)
	  port map (input         => inst.imm,
                sign          => '0',
                output        => inst.imm_uext);
  end block;
    
  CONTROLLERS : block
  begin    
    -- Control unit  
    CONTROL1: control
      port map (op_code       => inst.opcode,
                ID_ctrl       => ID_ctrl,
                EX_ctrl       => o.EX_ctrl, 
                MEM_ctrl      => o.MEM_ctrl, 
                WB_ctrl       => o.WB_ctrl);
    ALU_CTRL1: alu_ctrl
      port map (funct         => inst.funct,
                alu_op        => ID_ctrl.alu_op,
                alu_code      => o.ALU_code,
                alu_src1      => o.ALU_src1,
                jump_reg      => jr);
  end block;
  
  REGISTERS : block
  begin
    -- The pipeline register
    IFID_REG1 : IFID_reg
      port map (D             => i,
                Q             => r,
                WE            => s_nostall,
                reset         => r_flush,
                clock         => clock);  
                
    -- The register file
    ID_REGFILE : regfile
      port map (src1          => inst.rs,
                src2          => inst.rt,
                dst           => wb.dst,
                wdata         => wb.wdata,
                rdata1        => rdata1, 
                rdata2        => rdata2, 
                WE            => wb.reg_write, 
                reset         => reset);
  end block;
  
  FORWARDING : block
  begin
      -- Data Forward unit
    DATA_FWRD : dataforward_id
      port map(EXMEM_RegWrite => EXMEM_fwd.reg_write,
               EXMEM_Rd       => EXMEM_fwd.dst,
			      EX_RegWrite    => EX_fwd.reg_write,
               EX_Rd          => EX_fwd.dst,
               rs             => inst.rs,
               rt             => inst.rt,
               rs_src         => Rs_ctrl,
               rt_src         => Rt_ctrl);
    RDATA1_MUX : mux4_1
      generic map(N => 32)
      port map (iSel          => Rs_ctrl,
                iA            => rdata1,
                iB            => EXMEM_fwd.wdata,
                iC            => EX_fwd.wdata,
                iD            => x"00000000",
                O             => brn_data1);
    RDATA2_MUX : mux4_1
      generic map(N => 32)
      port map (iSel          => Rt_ctrl,
                iA            => rdata2,
                iB            => EXMEM_fwd.wdata,
                iC            => EX_fwd.wdata,
                iD            => x"00000000",
                O             => brn_data2);

  end block;
  
  DATA_HAZARD : block
  begin
    -- Jump/Branch hazard
	r_flush <= PC_info.PC_ctrl(0) or PC_info.PC_ctrl(1) or reset;
    -- Load-use hazard detection
    HAZARD_DETECTION : hazard_unit
      Port map (load_info     => load_info,
                rs            => inst.rs,
                rt            => inst.rt,
                nostall       => s_nostall);
  end block;
  
  BRANCH_CONTROL : block
  begin       
    --Branch_Logic
    BLU1 : blu
      port map(rdata1         => brn_data1,
               rdata2         => brn_data2,
               beq            => ID_Ctrl.beq,
               bne            => ID_Ctrl.bne,
               branchTaken    => branch_result);

    -- PC_Branch
    inst.branch         <= inst.imm_ext(29 downto 0) & "00";
    BRANCH_ADDER : adder
      port map(data1          => r.PC_4,
               data2          => inst.branch,
               result         => PC_info.PC_branch);
  
    -- PC_Jump
    PC_info.PC_jump  <= r.PC_4(31 downto 28) & inst.addr & "00";
  
    -- PC_jr
    PC_info.PC_jr    <= rdata1;
  
    -- PC_Ctrl
    PC_info.PC_ctrl  <= (ID_Ctrl.jump or jr) & branch_result;
  end block;  
  -- Pass NSPC, rs, rt, rd, and inst string
  o.inst_str            <= r.inst_str;
  o.PC_4                <= r.PC_4;
  o.rt_addr             <= inst.rt;
  o.rs_addr             <= inst.rs;
  o.rd_addr             <= inst.rd;
  o.rdata1              <= rdata1;
  o.rdata2              <= rdata2;
  o.shamt               <= inst.shamt;
  o.imm_ext             <= inst.imm_ext;
  o.imm_uext            <= inst.imm_uext;
  nostall               <= s_nostall;

end structure;
