-- idex_reg.vhd: The IDIE register
--
-- Zhao Zhang, CprE 381, fall 2013
--

library IEEE;
use IEEE.std_logic_1164.all;
use work.mips32.all;
use work.mips32pl.all;

entity IDEX_reg is
  port (D      : in  m32_IDEX;          -- Input from ID stage
        Q      : out m32_IDEX;          -- Output to EX stage
        WE     : in  m32_1bit;          -- Write enable
        reset  : in  m32_1bit;          -- The reset/flush signal
        clock  : in  m32_1bit);         -- The clock signal
end IDEX_reg;

architecture behavior of IDEX_reg is
begin
  REG : process (clock)
  begin
    if (rising_edge(clock)) then
      if (reset = '1') then             -- Reset branch/jump, reg_write, mem_write to zeros
        Q.EX_ctrl.alu_src2    <= "00";
        Q.EX_ctrl.reg_dst     <= "00";
        Q.MEM_ctrl.mem_read   <= '0';  
        Q.MEM_ctrl.mem_write  <= '0';
        Q.MEM_ctrl.mem_size   <= "00";
        Q.WB_ctrl.reg_write   <= '0';
        Q.WB_ctrl.mem_to_reg  <= '0';
        Q.inst_str  <= NOP;
      elsif (WE = '1') then
        Q <= D;
      end if;
    end if;
  end process;
end behavior;
