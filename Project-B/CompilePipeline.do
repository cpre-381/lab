vlib work
vcom -reportprogress 300 -work work {Project-B/PipelineStages/if_stage.vhd}
vcom -reportprogress 300 -work work {Project-B/PipelineStages/ifid_reg.vhd}
vcom -reportprogress 300 -work work {Project-B/PipelineStages/id_stage.vhd}
vcom -reportprogress 300 -work work {Project-B/PipelineStages/idex_reg.vhd}
vcom -reportprogress 300 -work work {Project-B/PipelineStages/ex_stage.vhd}
vcom -reportprogress 300 -work work {Project-B/PipelineStages/exmem_reg.vhd}
vcom -reportprogress 300 -work work {Project-B/PipelineStages/mem_stage.vhd}
vcom -reportprogress 300 -work work {Project-B/PipelineStages/memwb_reg.vhd}
vcom -reportprogress 300 -work work {Project-B/PipelineStages/wb_stage.vhd}
vcom -reportprogress 300 -work work {Project-B/PipelineStages/tb_if.vhd}
vcom -reportprogress 300 -work work {Project-B/PipelineStages/tb_id.vhd}
vcom -reportprogress 300 -work work {Project-B/PipelineStages/tb_ex.vhd}
vcom -reportprogress 300 -work work {Project-B/PipelineStages/tb_mem.vhd}
vcom -reportprogress 300 -work work {Project-B/PipelineStages/tb_wb.vhd}