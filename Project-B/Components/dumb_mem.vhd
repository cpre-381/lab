-------------------------------------------------------------------------
-- Justin Rilling
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- mem.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains a behavioral model of a generic memory 
-- 
--
--
-- NOTES:
-- 10/10/10 by JRR::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
use std.textio.all;

entity mem is
	generic(depth_exp_of_2 	: integer := 10;
      mif_filename 	: string := "mem.mif");
	port   (address			: IN STD_LOGIC_VECTOR (depth_exp_of_2-1 DOWNTO 0) := (OTHERS => '0');
      byteena			: IN STD_LOGIC_VECTOR (3 DOWNTO 0) := (OTHERS => '1');
			clock			: IN STD_LOGIC := '1';
			data			: IN STD_LOGIC_VECTOR (31 DOWNTO 0) := (OTHERS => '0');
			wren			: IN STD_LOGIC := '0';
			q				: OUT STD_LOGIC_VECTOR (31 DOWNTO 0));         
end entity mem;

architecture behavioral of mem is
begin
  q <= x"FFFFFFFF";
end architecture behavioral;
