library IEEE;
use IEEE.std_logic_1164.all;
use work.mips32.all;

entity mux4_1 is
  generic(N : Integer := 32);
  port(iSel : in m32_2bits;
       iA     : in  std_logic_vector(N-1 downto 0);
       iB     : in  std_logic_vector(N-1 downto 0);
       iC     : in  std_logic_vector(N-1 downto 0);
       iD     : in  std_logic_vector(N-1 downto 0);
       O      : out std_logic_vector(N-1 downto 0));
end mux4_1;

architecture behavior of mux4_1 is
  begin
    O <= iA when iSel = "00" else
         iB when iSel = "01" else
         iC when iSel = "10" else
         iD when iSel = "11";
      
end behavior;