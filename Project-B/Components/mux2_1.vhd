------------------------------------------------------------------------
-- Ryan Wade
-- CPRE 381
-- Iowa State University
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.mips32.all;

entity mux2_1 is
  generic(N : Integer := 32);
  port(iSel : in m32_1bit;
       iA     : in  std_logic_vector(N-1 downto 0);
       iB     : in  std_logic_vector(N-1 downto 0);
       O      : out std_logic_vector(N-1 downto 0));
end mux2_1;

architecture behavior of mux2_1 is
  begin
    O <= iA when iSel = '0' else
         iB when iSel = '1';
      
end behavior;