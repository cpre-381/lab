-- control.vhd: CprE 381 S15 template file
-- Zhao Zhang, Spring 2015
-- 
-- The main control unit of MIPS
-- 

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.mips32.all;
use work.mips32pl.all;

entity control is
  port (op_code     : in  m32_6bits;
        ID_ctrl     : out m32_ID_ctrl;
        EX_ctrl     : out m32_EX_ctrl;
        MEM_ctrl    : out m32_MEM_ctrl;
        WB_ctrl     : out m32_WB_ctrl);
end control;

architecture behavior of control is
    signal delayed_op_code : m32_6bits;
begin
  -- The opcode translation takes 80% CCT
  delayed_op_code <= op_code after 0.8*CCT;

  -- The translation process with the delayed oopcode
  CTRL_P : process (delayed_op_code)
    variable op_num 		: integer;
  begin
    op_num := to_integer(unsigned(op_code));

    -- Branch/Jump encoding
    --   0000: no branch
    --   0010: beq
    --   0100: bne
    --   1000: jump
    
    -- ALU OP
    --   000: LW/SW and other I-type (excluding the following). 
    --        Note: Don't use '---' for other I-type,
    --        because it may triger jr signal in mistake.
    --   001: ORI
    --       Note: It was for BNE/BEQ. This pipeline imlementation
    --       does not use the ALU for branch.
    --   010: R-type (funct decides ALU operation)
    --   011: ANDI
    --   100: SLTI
    
    -- ALU source
    --   00: rt
    --   01: imm
    --   10: lui
    --   11: not used
    
    -- Register destination
    --   00: rt
    --   01: rd
    --   11: 31 fixed
    
    -- Memory (operand) size
    --   00: Byte
    --   01: Halfword
    --   10: Word
    --   11: Not used

    case op_num is
      when 0 =>     -- R-type
        ID_ctrl <= (beq => '0', bne => '0', bgtz => '0', blez => '0', jump => '0',
                    alu_op => "010", jal => '0');
        EX_ctrl <= (alu_src2 => "00", reg_dst => "01");
        MEM_ctrl <= (mem_read => '0', mem_write => '0', mem_size => "--");
        WB_ctrl <= (reg_write => '1', mem_to_reg => '0');
      when 2 =>     -- J
        ID_ctrl <= (beq => '0', bne => '0', bgtz => '0', blez => '0', jump => '1', 
                    alu_op => "000", jal => '0');
        EX_ctrl <= (alu_src2 => "--", reg_dst => "--");
        MEM_ctrl <= (mem_read => '0', mem_write => '0', mem_size => "--");
        WB_ctrl <= (reg_write => '0', mem_to_reg => '-');
      when 3 =>     -- JAL
        ID_ctrl <= (beq => '0', bne => '0', bgtz => '0', blez => '0', jump => '1',
                    alu_op => "000", jal => '1');
        EX_ctrl <= (alu_src2 => "--", reg_dst => "11");
        MEM_ctrl <= (mem_read => '0', mem_write => '0', mem_size => "--");
        WB_ctrl <= (reg_write => '1', mem_to_reg => '0');
      when 4 =>     -- BEQ
        ID_ctrl <= (beq => '1', bne => '0', bgtz => '0', blez => '0', jump => '0',
                    alu_op => "000", jal => '0');
        EX_ctrl <= (alu_src2 => "--", reg_dst => "--");
        MEM_ctrl <= (mem_read => '0', mem_write => '0', mem_size => "--");
        WB_ctrl <= (reg_write => '0', mem_to_reg => '-');
      when 5 =>     -- BNE
        ID_ctrl <= (beq => '0', bne => '1', bgtz => '0', blez => '0', jump => '0',
                    alu_op => "000", jal => '0');
        EX_ctrl <= (alu_src2 => "00", reg_dst => "--");
        MEM_ctrl <= (mem_read => '0', mem_write => '0', mem_size => "--");
        WB_ctrl <= (reg_write => '0', mem_to_reg => '-');
      when 6 =>     -- BLEZ
        ID_ctrl <= (beq => '0', bne => '0', bgtz => '0', blez => '1', jump => '0',
                    alu_op => "000", jal => '0');
        EX_ctrl <= (alu_src2 => "00", reg_dst => "--");
        MEM_ctrl <= (mem_read => '0', mem_write => '0', mem_size => "--");
        WB_ctrl <= (reg_write => '0', mem_to_reg => '-');
      when 7 =>     -- BGTZ
        ID_ctrl <= (beq => '0', bne => '0', bgtz => '1', blez => '0', jump => '0',
                    alu_op => "000", jal => '0');
        EX_ctrl <= (alu_src2 => "00", reg_dst => "--");
        MEM_ctrl <= (mem_read => '0', mem_write => '0', mem_size => "--");
        WB_ctrl <= (reg_write => '0', mem_to_reg => '-');
      when 8  =>    -- ADDI
        ID_ctrl <= (beq => '0', bne => '0', bgtz => '0', blez => '0', jump => '0',
                    alu_op => "000", jal => '0');
        EX_ctrl <= (alu_src2 => "01", reg_dst => "00");
        MEM_ctrl <= (mem_read => '0', mem_write => '0', mem_size => "--");
        WB_ctrl <= (reg_write => '1', mem_to_reg => '0');
      when 9  =>    -- ADDIU, no difference yet with ADDI
        ID_ctrl <= (beq => '0', bne => '0', bgtz => '0', blez => '0', jump => '0',
                    alu_op => "000", jal => '0');
        EX_ctrl <= (alu_src2 => "01", reg_dst => "00");
        MEM_ctrl <= (mem_read => '0', mem_write => '0', mem_size => "--");
        WB_ctrl <= (reg_write => '1', mem_to_reg => '0');
      when 10 =>    -- SLTI
        ID_ctrl <= (beq => '0', bne => '0', bgtz => '0', blez => '0', jump => '0',
                    alu_op => "100", jal => '0');
        EX_ctrl <= (alu_src2 => "01", reg_dst => "00");
        MEM_ctrl <= (mem_read => '0', mem_write => '0', mem_size => "--");
        WB_ctrl <= (reg_write => '1', mem_to_reg => '0');      
      when 13  =>   -- ORI
        ID_ctrl <= (beq => '0', bne => '0', bgtz => '0', blez => '0', jump => '0',
                    alu_op => "001", jal => '0');
        EX_ctrl <= (alu_src2 => "01", reg_dst => "00");
        MEM_ctrl <= (mem_read => '0', mem_write => '0', mem_size => "--");
        WB_ctrl <= (reg_write => '1', mem_to_reg => '0');
      when 15 =>    -- LUI
        ID_ctrl <= (beq => '0', bne => '0', bgtz => '0', blez => '0', jump => '0',
                    alu_op => "000", jal => '0');
        EX_ctrl <= (alu_src2 => "10", reg_dst => "00");
        MEM_ctrl <= (mem_read => '0', mem_write => '0', mem_size => "--");
        WB_ctrl <= (reg_write => '1', mem_to_reg => '0');
      when 35 =>    -- LW
        ID_ctrl <= (beq => '0', bne => '0', bgtz => '0', blez => '0', jump => '0',
                    alu_op => "000", jal => '0');
        EX_ctrl <= (alu_src2 => "01", reg_dst => "00");
        MEM_ctrl <= (mem_read => '1', mem_write => '0', mem_size => "10");
        WB_ctrl <= (reg_write => '1', mem_to_reg => '1');
      when 36 =>    -- LBU
        ID_ctrl <= (beq => '0', bne => '0', bgtz => '0', blez => '0', jump => '0',
                    alu_op => "000", jal => '0');
        EX_ctrl <= (alu_src2 => "01", reg_dst => "00");
        MEM_ctrl <= (mem_read => '1', mem_write => '0', mem_size => "00");
        WB_ctrl <= (reg_write => '1', mem_to_reg => '1');
      when 37 =>    -- LHU
        ID_ctrl <= (beq => '0', bne => '0', bgtz => '0', blez => '0', jump => '0',
                    alu_op => "000", jal => '0');
        EX_ctrl <= (alu_src2 => "01", reg_dst => "00");
        MEM_ctrl <= (mem_read => '1', mem_write => '0', mem_size => "01");
        WB_ctrl <= (reg_write => '1', mem_to_reg => '1');
      when 40 =>    -- SB
        ID_ctrl <= (beq => '0', bne => '0', bgtz => '0', blez => '0', jump => '0',
                    alu_op => "000", jal => '0');
        EX_ctrl <= (alu_src2 => "01", reg_dst => "00");
        MEM_ctrl <= (mem_read => '0', mem_write => '1', mem_size => "00");
        WB_ctrl <= (reg_write => '0', mem_to_reg => '-');
      when 41 =>    -- SH
        ID_ctrl <= (beq => '0', bne => '0', bgtz => '0', blez => '0', jump => '0',
                    alu_op => "000", jal => '0');
        EX_ctrl <= (alu_src2 => "01", reg_dst => "00");
        MEM_ctrl <= (mem_read => '0', mem_write => '1', mem_size => "01");
        WB_ctrl <= (reg_write => '0', mem_to_reg => '-');
      when 43 =>    -- SW
        ID_ctrl <= (beq => '0', bne => '0', bgtz => '0', blez => '0', jump => '0',
                    alu_op => "000", jal => '0');
        EX_ctrl <= (alu_src2 => "01", reg_dst => "--");
        MEM_ctrl <= (mem_read => '0', mem_write => '1', mem_size => "10");
        WB_ctrl <= (reg_write => '0', mem_to_reg => '-');
      when others =>
        ID_ctrl <= (beq => '0', bne => '0', bgtz => '0', blez => '0', jump => '0',
                    alu_op => "000", jal => '0');
        EX_ctrl <= (alu_src2 => "00", reg_dst => "00");
        MEM_ctrl <= (mem_read => '0', mem_write => '0', mem_size => "00");
        WB_ctrl <= (reg_write => '0', mem_to_reg => '0');
    end case;
  end process;
end behavior;
