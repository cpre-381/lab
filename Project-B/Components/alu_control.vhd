-- alu.vhd
-- 
-- The ALU unit
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.mips32.all;

entity alu_ctrl is
  port (funct       : in  m32_6bits;
        alu_op      : in  m32_3bits;
        alu_code    : out m32_4bits;
        alu_src1    : out m32_1bit;
        jump_reg    : out m32_1bit);
end entity;

architecture behavior of alu_ctrl is
  signal r : m32_word;    
begin
  P_Control : process (alu_op, funct)
  begin
    jump_reg <= '0';
    -- Select the result, convert to signal if necessary
    case (alu_op) is
      when "000" =>               -- LW/SW and other I-type
        alu_code <= "0010";
        alu_src1 <= '0';
      when "001" =>               -- ORI (it was used for branch but Branch is no handled by the BLU (Branch Logic Unit)
        alu_code <= "0001";
        alu_src1 <= '0';
      when "010" =>               -- R-type (funct decides ALU operation)
        case(funct) is
          when "100100" =>        --AND
            alu_code <= "0000";
            alu_src1 <= '0';
          when "100101" =>        --OR
            alu_code <= "0001";
            alu_src1 <= '0';
--          when "000000" =>      --XOR
--            alu_code <= "1101";
--            alu_src1 <= '0';
          when "100000" =>        --ADD
            alu_code <= "0010";
            alu_src1 <= '0';
          when "000000" =>        --SLL
            alu_code <= "0011";
			      alu_src1 <= '1';
--			    when "000000" =>        --SRL
--			      alu_code <= "1011";
--			      alu_src1 <= '0';
--        when "000000" =>        --SRA
--          alu_code <= "1001"
--          alu_src1 <= '0';
          when "100010" =>        --SUB
            alu_code <= "0110";
            alu_src1 <= '0';            
          when "101010" =>        --SLT
            alu_code <= "0111";
            alu_src1 <= '0';
          when "100111" =>        --NOR
            alu_code <= "1100";
            alu_src1 <= '0';            
			    when "001000" =>        --JR
			      alu_code <= "1111";
            jump_reg <= '1';
            alu_src1 <= '0';
          when others =>          --Others??
            alu_code <= "1111";
            alu_src1 <= '0';
        end case;
      when "011" =>	              --ANDI
        alu_code <= "0000";
        alu_src1 <= '0';
      when "100" =>               --SLTI
        alu_code <= "0111";
        alu_src1 <= '0';
      when others =>      -- Otherwise, make output to be 0
        alu_code <= "1111";
        alu_src1 <= '0';
    end case;
  end process;
end behavior;