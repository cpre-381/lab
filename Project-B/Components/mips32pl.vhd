-- mips32pl.vhd: Package for MIPS32 pipeline implementation in CprE 381
--
-- Zhao Zhang, Fall 2013
--

library IEEE;
use IEEE.std_logic_1164.all;
use work.mips32.all;

-- The types used in the pipeline modeling
package mips32pl is
  -- EX stage control signals
  type m32_ID_ctrl is
    record
      alu_op      : m32_3bits;
      jal         : m32_1bit;
      beq         : m32_1bit;
      bne         : m32_1bit;
      bgtz        : m32_1bit;
      blez        : m32_1bit;
      jump        : m32_1bit;    -- if != 0, flush IF/ID
    end record;
  type m32_EX_ctrl is
    record
      alu_src2    : m32_2bits;
      reg_dst     : m32_2bits;
    end record;
  
  -- MEM stage control signals
  type m32_MEM_ctrl is
    record
      mem_read    : m32_1bit;
      mem_write   : m32_1bit;
      mem_size    : m32_2bits;
    end record;
  
  -- WB stage control signals
  type m32_WB_ctrl is
    record
      reg_write   : m32_1bit;
      mem_to_reg  : m32_1bit;
    end record;
  
  -- All control signals
  type m32_ctrl is
    record
      ID_ctrl   : m32_ID_ctrl;
      EX_ctrl   : m32_EX_ctrl;
      MEM_ctrl  : m32_MEM_ctrl;
      WB_ctrl   : m32_WB_ctrl;
    end record;
  
  -- The IF/ID register type
  type m32_IFID is
    record        
      -- Instruction string, for debugging
      inst_str  : m32_inst_str;
      
      PC_4      : m32_word;     -- Next sequential PC
      inst      : m32_word;
    end record;    
  
  -- The ID/EX register type
  type m32_IDEX is
    record
      -- Instruction string, for debugging
      inst_str  : m32_inst_str;
      
      -- Control signals
      EX_ctrl   : m32_EX_ctrl;
      MEM_ctrl  : m32_MEM_ctrl;
      WB_ctrl   : m32_WB_ctrl;
      
      ALU_code  : m32_4bits;
      ALU_src1  : m32_1bit;
      -- Next sequetial PC
      PC_4      : m32_word;

      -- 5 -bit register addresses
      rt_addr   : m32_5bits;
	  rs_addr   : m32_5bits;
      rd_addr   : m32_5bits;
      -- 32-bit register/immediate values
      rdata1    : m32_word;
      rdata2    : m32_word;
	  shamt     : m32_word;
      imm_ext   : m32_word;
	  imm_uext  : m32_word;
    end record;
    
  -- The EX/MEM register type
  type m32_EXMEM is
    record
      -- Instruction string, for debugging
      inst_str  : m32_inst_str;
      
      -- Control signals
      MEM_ctrl    : m32_MEM_ctrl;
      WB_ctrl     : m32_WB_ctrl;
           
      -- 32-bit register values
      alu_result  : m32_word;
      rdata2      : m32_word;
      
      -- Register numbers
      dst         : m32_5bits;
    end record;
    
  -- The MEM/WB register type
  type m32_MEMWB is
    record
      -- Instruction string, for debugging
      inst_str  : m32_inst_str;
      
      -- Control signals
      WB_ctrl     : m32_WB_ctrl;
      
      -- ALU result
      alu_result  : m32_word;
      
      -- Register number
      dst         : m32_5bits;
      
      -- Memory result
      mem_result  : m32_word;
    end record;
    
  -- The output from the WB stage
  type m32_WB is
    record
      reg_write : m32_1bit;   -- Register write signal
      dst       : m32_5bits;  -- Register number to write
      wdata     : m32_word;   -- Reigster data to write
    end record;
    
  -- The forwarding data type, from MEM and WB to EX
  type m32_fwd is
    record
      reg_write : m32_1bit;
      dst       : m32_5bits;
      wdata     : m32_word;
    end record;
  
  -- Load information, from EX to ID
  type m32_load_info is
    record
      is_load   : m32_1bit;   -- If the instruction is a load
      rt        : m32_5bits;  -- Register destination
    end record;
  
  -- Branch outcome
  type m32_PC_info is
    record
      PC_branch : m32_word;
      PC_jump   : m32_word;
      PC_jr     : m32_word;
      PC_ctrl   : m32_2bits;
    end record;
  type m32_inst is
    record
      opcode     : m32_6bits;
	  shamt      : m32_word;
      funct      : m32_6bits;
      rs         : m32_5bits;
      rt         : m32_5bits;
      rd         : m32_5bits;
      imm        : m32_halfword;
      imm_ext    : m32_word;
	  imm_uext   : m32_word;
      branch     : m32_word;
      addr       : m32_26bits;
    end record;
  type m32_dmem_ctrl is
    record
	  addr       : m32_word;     -- Data memory address
      re         : m32_1bit;     -- Data memory read?
      we         : m32_1bit;     -- Data memory write?
      wmask      : m32_4bits;    -- Data memory write mask
      wdata      : m32_word;     -- Data memory write data
	end record;
end package;
