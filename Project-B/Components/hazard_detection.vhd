library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.mips32.all;
use work.mips32pl.all;

entity hazard_unit is
  port(load_info       : in m32_load_info;
       rs                 : in m32_5bits;
       rt                 : in m32_5bits;
       nostall     : out m32_1bit);
end hazard_unit;

architecture behavior of hazard_unit is
signal eq0 : m32_1bit;
signal eq1 : m32_1bit;
signal eq2 : m32_1bit;
begin
  process(load_info.rt)
  begin
    if(load_info.rt = "00000") then
	   eq0 <= '1';
	 else
	   eq0 <= '0';
	 end if;
	end process;
  process(load_info.rt, rs)
  begin
    if(load_info.rt = rs) then
	   eq1 <= '1';
	 else
	   eq1 <= '0';
	 end if;
	end process;
	process(load_info.rt,rt)
	begin
	  if(load_info.rt = rt) then
	    eq2 <= '1';
	  else
	    eq2 <= '0';
	  end if;
   end process;
	nostall <= (NOT ((eq1 or eq2) and load_info.is_Load)) or eq0;
end behavior;