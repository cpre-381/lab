-- alu.vhd
-- The ALU unit
--
-- Sample file for MIPS pipeline implementation
-- Zhao Zhang, Spring 2015
--

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--use IEEE.std_logic_arith.all;
use work.mips32.all;

entity ALU is
  port (data1       : in  m32_word;
        data2       : in  m32_word;
        alu_code    : in  m32_4bits;
        result      : out m32_word);
end entity;

architecture behavior of ALU is
  signal r : m32_word;
begin
  P_ALU : process (alu_code, data1, data2)
    variable code, a, b, sum, diff, slt : integer;
    variable sll_result : bit_vector(31 downto 0);
    variable srl_result : bit_vector(31 downto 0);
    variable sra_result : bit_vector(31 downto 0);
  begin
    -- Pre-calculate arithmetic results
    a := to_integer(signed(data1));
    b := to_integer(signed(data2));
    sum := a + b;
    diff := a - b;
    if (a < b) then 
      slt := 1; 
    else 
      slt := 0;
    end if;
    sll_result := to_bitvector(data2) sll a;
    srl_result := to_bitvector(data2) srl a;
    sra_result := to_bitvector(data2) sra a;
    
    -- Select the result, convert to signal if necessary
    case alu_code is
      when "0000" =>      -- AND
        r <= data1 AND data2;
      when "0001" =>      -- OR
        r <= data1 OR data2;
      when "0010" =>      -- ADD
        r <= std_logic_vector(to_signed(sum, 32));
      when "0110" =>      -- SUB
        r <= std_logic_vector(to_signed(diff, 32));
      when "0111" =>      -- SLT
        r <= std_logic_vector(to_unsigned(slt, 32));
      when "1100" =>      -- NOR
        r <= data1 NOR data2;
      when "1101" =>      -- XOR
        r <= data1 XOR data2;
      when "0011" =>      -- SLL
        r <= to_stdlogicvector(sll_result);
      when "1011" =>      -- SRL
        r <= to_stdlogicvector(srl_result);
      when "1001" =>      -- SRA
        r <= to_stdlogicvector(sra_result);
      when others =>      -- Otherwise, make output to be 0
        r <= (others => '0');
    end case;
  end process;
  
  -- Drive the alu result output after 95% CCT
  result <= r after 0.95*CCT;

end behavior;
