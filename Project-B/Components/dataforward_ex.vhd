library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.mips32.all;

entity dataforward_ex is
  port(EXMEM_RegWrite  : in  m32_1bit;
       MEMWB_RegWrite  : in  m32_1bit;
       EXMEM_Rd        : in  m32_5bits;
       MEMWB_Rd        : in  m32_5bits;
       rs              : in  m32_5bits;
       rt              : in  m32_5bits;
       rs_src          : out m32_2bits;
       rt_src          : out m32_2bits);
end dataforward_ex;

architecture behavior of dataforward_ex is
  begin
    process(EXMEM_RegWrite,EXMEM_Rd,MEMWB_Rd,MEMWB_RegWrite,rs,rt)
      begin
        --Default the signals
        rs_src  <= "00";
        rt_src<= "00";
          
        --Forward Rs hazard
        if((EXMEM_RegWrite = '1') and (Not(EXMEM_Rd = "00000")) and (EXMEM_Rd = rs)) Then
          rs_src <= "01";
        elsif ((MEMWB_RegWrite = '1') and (not(MEMWB_Rd = "00000")) and (MEMWB_Rd = rs)) Then
          rs_src <= "10";
        end if;
        
        --Forward Rt hazard
        if ((EXMEM_RegWrite = '1') and (not(EXMEM_Rd = "00000")) and (EXMEM_Rd = rt)) Then
          rt_src <= "01";
        elsif ((MEMWB_RegWrite = '1') and (not(MEMWB_Rd = "00000")) and (MEMWB_Rd = rt)) Then
          rt_src <= "10";
        end if;
		  
      end process; 
end behavior;
