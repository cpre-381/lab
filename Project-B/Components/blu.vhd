library IEEE;
use IEEE.std_logic_1164.all;
use work.mips32.all;
use IEEE.numeric_std.all;


--Branch logic unit
 --rdata1, rdata2, check if equals
 --check if beq or bne and result to set output
entity blu is
  port(rdata1      : in m32_word;
       rdata2      : in m32_word;
       beq         : in m32_1bit;
       bne         : in m32_1bit;
       branchTaken : out m32_1bit);
end blu;

architecture behavioral of blu is
  signal result : m32_1bit;
  begin
    process (rdata1, rdata2)
      begin
        if(rdata1=rdata2) then
          result <= '1';
        else
          result <= '0';
        end if;
   end process; 
   branchtaken <= (beq and result) or (bne and not(result));
end behavioral;