-- regfile.vhd: Register file for the MIPS processor
--
-- Zhao Zhang, Fall 2013
--

--
-- MIPS regfile, unclocked version for MIPS pipeline
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.mips32.all;

entity regfile is
  port(src1   : in  m32_5bits;
       src2   : in  m32_5bits;
       dst    : in  m32_5bits;
       wdata  : in  m32_word;
       rdata1 : out m32_word;
       rdata2 : out m32_word;
       WE     : in  m32_1bit;
       reset  : in  m32_1bit);
end regfile;

architecture behavior of regfile is
  signal reg_array : m32_regval_array;
begin
  -- Register reset/write logic, unclocked
  P_WRITE : process (reset, WE, dst, wdata)
    variable r : integer;
  begin
    -- Reset logic
    if (reset = '1') then
      reg_array <= (reg_array'range => X"00000000");
    end if;

    -- Write logic
    if (WE = '1') then
      r := to_integer(unsigned(dst));
      if not (r = 0) then         -- MIPS $0 stores 0
         reg_array(r) <= inertial wdata after 0.45*CCT;
      end if;
    end if;
  end process;

  -- Register read logic 
  P_READ : process (reg_array, src1, src2)
    variable r1, r2 : integer;
  begin
    r1 := to_integer(unsigned(src1));
    r2 := to_integer(unsigned(src2));
    rdata1 <= transport reg_array(r1) after 0.45*CCT;	-- Read takes 0.45 CCT
    rdata2 <= transport reg_array(r2) after 0.45*CCT;	-- Read takes 0.45 CCT
  end process;
end behavior;
