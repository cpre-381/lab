onerror {resume}
vsim -gui work.tb_if_stage
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group PC_Control -radix decimal /tb_if_stage/t_o.PC_4
add wave -noupdate -expand -group PC_Control -radix decimal /tb_if_stage/t_i.PC_branch
add wave -noupdate -expand -group PC_Control -radix decimal /tb_if_stage/t_i.PC_jump
add wave -noupdate -expand -group PC_Control -radix decimal /tb_if_stage/t_i.PC_jr
add wave -noupdate -expand -group PC_Control /tb_if_stage/t_i.PC_ctrl
add wave -noupdate -expand -group Instruction -radix decimal /tb_if_stage/imem_addr
add wave -noupdate -expand -group Instruction -radix decimal /tb_if_stage/t_o.inst
add wave -noupdate -expand -group Instruction /tb_if_stage/t_o.inst_str
add wave -noupdate -radix decimal /tb_if_stage/nostall
add wave -noupdate -radix decimal /tb_if_stage/reset
add wave -noupdate -radix decimal /tb_if_stage/clock
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 184
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 100000
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {100 ns}
update
run 2 us
update