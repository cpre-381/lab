vcom -reportprogress 300 -work work {Project-B/Components/mips32.vhd}
vcom -reportprogress 300 -work work {Project-B/Components/mips32pl.vhd}

vcom -reportprogress 300 -work work {Project-B/Components/alu_ctrl.vhd}
vcom -reportprogress 300 -work work {Project-B/Components/control.vhd}

vcom -reportprogress 300 -work work {Project-B/Components/dataforward_ex.vhd}
vcom -reportprogress 300 -work work {Project-B/Components/dataforward_id.vhd}
vcom -reportprogress 300 -work work {Project-B/Components/hazard_unit.vhd}

vcom -reportprogress 300 -work work {Project-B/Components/alu.vhd}
vcom -reportprogress 300 -work work {Project-B/Components/blu.vhd}
vcom -reportprogress 300 -work work {Project-B/Components/adder.vhd}

vcom -reportprogress 300 -work work {Project-B/Components/extender.vhd}
vcom -reportprogress 300 -work work {Project-B/Components/mux4_1.vhd}
vcom -reportprogress 300 -work work {Project-B/Components/mux2_1.vhd}

vcom -reportprogress 300 -work work {Project-B/Components/mem.vhd}
vcom -reportprogress 300 -work work {Project-B/Components/reg.vhd}
vcom -reportprogress 300 -work work {Project-B/Components/regfile.vhd}

vcom -reportprogress 300 -work work {Project-B/Components/instruction_decoder.vhd}