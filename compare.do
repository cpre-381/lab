onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_cpu/A_inst_str
add wave -noupdate -radix decimal /tb_cpu/A_imem_addr
add wave -noupdate /tb_cpu/B_inst_str
add wave -noupdate -radix decimal /tb_cpu/imem_addr
add wave -noupdate -group DMEM -radix decimal /tb_cpu/PROJECTA_CPU/A_DATA_MEM/mem(7)
add wave -noupdate -group DMEM -radix decimal /tb_cpu/PROJECTB_CPU/DATA_MEM/mem(7)
add wave -noupdate -group DMEM -radix decimal /tb_cpu/PROJECTA_CPU/A_DATA_MEM/mem(6)
add wave -noupdate -group DMEM -radix decimal /tb_cpu/PROJECTB_CPU/DATA_MEM/mem(6)
add wave -noupdate -group DMEM -radix decimal /tb_cpu/PROJECTA_CPU/A_DATA_MEM/mem(5)
add wave -noupdate -group DMEM -radix decimal /tb_cpu/PROJECTB_CPU/DATA_MEM/mem(5)
add wave -noupdate -group DMEM -radix decimal /tb_cpu/PROJECTA_CPU/A_DATA_MEM/mem(4)
add wave -noupdate -group DMEM -radix decimal /tb_cpu/PROJECTB_CPU/DATA_MEM/mem(4)
add wave -noupdate -group DMEM -radix decimal /tb_cpu/PROJECTA_CPU/A_DATA_MEM/mem(3)
add wave -noupdate -group DMEM -radix decimal /tb_cpu/PROJECTB_CPU/DATA_MEM/mem(3)
add wave -noupdate -group DMEM -radix decimal /tb_cpu/PROJECTA_CPU/A_DATA_MEM/mem(2)
add wave -noupdate -group DMEM -radix decimal /tb_cpu/PROJECTB_CPU/DATA_MEM/mem(2)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(0)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(0)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(1)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(1)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(2)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(2)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(3)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(3)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(4)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(4)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(5)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(5)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(6)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(6)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(7)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(7)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(8)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(8)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(9)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(9)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(10)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(10)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(11)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(11)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(12)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(12)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(13)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(13)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(14)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(14)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(15)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(15)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(16)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(16)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(17)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(17)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(18)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(18)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(19)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(19)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(20)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(20)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(21)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(21)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(22)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(22)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(23)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(23)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(24)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(24)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(25)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(25)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(26)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(26)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(27)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(27)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(28)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(28)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(29)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(29)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(30)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(30)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTA_CPU/A_CPU1/REGFILE1/reg_array(31)
add wave -noupdate -expand -group Registers -radix decimal /tb_cpu/PROJECTB_CPU/id_stage_2/REGISTERS/ID_REGFILE/reg_array(31)
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {43553 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 489
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {101437 ps}
