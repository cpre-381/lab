-------------------------------------------------------------------------
-- Ryan Wade
-- CPRE 381
-- Iowa State University
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_signed.all;
use IEEE.numeric_std.all;
use work.register_array_type.all;

entity tb_regFile is
  generic(gCLK_HPER   : time := 50 ns);
end tb_regFile;

architecture behavior of tb_regFile is
  
  -- Calculate the clock period as twice the half-period
  constant cCLK_PER  : time := gCLK_HPER * 2;

  component Register_File
    generic(N: integer := 32);
    port(i_CLK	    : in std_logic;
         i_RST	    : in std_logic;
         rt	    : in std_logic_vector(4 downto 0);
         rd	    : in std_logic_vector(4 downto 0);
         rw	    : in std_logic_vector(4 downto 0);
         WE         : in std_logic;
         Data         : in std_logic_vector(N-1 downto 0);
         o_rt         : out std_logic_vector(N-1 downto 0);
         o_rd         : out std_logic_vector(N-1 downto 0));
  end component;
  -- Temporary signals to connect to the dff component.
  signal s_A : registerArray := (x"00000000",x"FFFFFFFF",x"FEFEFEFE",x"FDFDFDFD",x"FCFCFCFC",x"FBFBFBFB",x"FAFAFAFA",x"F9F9F9F9",x"F8F8F8F8",x"F7F7F7F7",x"F6F6F6F6",x"F5F5F5F5",x"F4F4F4F4",x"F3F3F3F3",x"F2F2F2F2",x"F1F1F1F1",x"F0F0F0F0",x"E0E0E0E0",x"D0D0D0D0",x"C0C0C0C0",x"B0B0B0B0",x"A0A0A0A0",x"90909090",x"80808080",x"70707070",x"60606060",x"50505050",x"40404040",x"30303030",x"20202020",x"10101010",x"AAAAAAAA");
  signal s_CLK : std_logic;
  signal s_RST : std_logic;
  signal s_rt : std_logic_vector(4 downto 0) := "00000";
  signal s_rd : std_logic_vector(4 downto 0) := "00000";
  signal s_rw : std_logic_vector(4 downto 0) := "00000";
  signal s_WE : std_logic := '0';
  signal rt : std_logic_vector(31 downto 0) := x"00000000";
  signal rd : std_logic_vector(31 downto 0) := x"00000000";
  signal w_data : std_logic_vector(31 downto 0) := x"00000000";

begin

  regFile: Register_File
  port map(s_CLK, s_RST, s_rt, s_rd, s_rw, s_WE, w_data, rt, rd);

  -- This process sets the clock value (low for gCLK_HPER, then high
  -- for gCLK_HPER). Absent a "wait" command, processes restart 
  -- at the beginning once they have reached the final statement.
  P_CLK: process
  begin
    s_CLK <= '0';
    wait for gCLK_HPER;
    s_CLK <= '1';
    wait for gCLK_HPER;
  end process;
  
  -- Testbench process  
  P_TB: process
  begin

    s_RST <= '1';
    wait for cCLK_PER;

    s_RST <= '0';
    w_data <= x"00000004";
    s_rw <= "00001";
    s_WE <= '1';
    wait for cCLK_PER;

    w_data <= x"00000006";
    s_rw <= "00010";
    s_WE <= '1';
    wait for cCLK_PER;

    s_rt <= "00001";
    s_rd <= "00010";
    s_WE <= '0';
    wait for cCLK_PER;

    w_data <= rt + rd;
    s_rw <= "00011";
    s_WE <= '1';
    wait for cCLK_PER;

    s_rt <= "00011";
    s_rd <= "00000";
    s_WE <= '0';
    wait for cCLK_PER;

    w_data <= x"00000006";
    s_rw <= "00000";
    s_WE <= '1';
    wait for cCLK_PER;


    s_rt <= "00011";
    s_rd <= "00000";
    s_WE <= '0';
    wait for cCLK_PER;   



    wait;
  end process;
  
end behavior;