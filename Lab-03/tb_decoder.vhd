-------------------------------------------------------------------------
-- Joseph Zambreno
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- tb_dff.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains a simple VHDL testbench for the
-- edge-triggered flip-flop with parallel access and reset.
--
--
-- NOTES:
-- 9/07/08 by JAZ::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.register_array_type.all;

entity tb_decoder is
  generic(gCLK_HPER   : time := 50 ns);
end tb_decoder;

architecture behavior of tb_decoder is
  
  -- Calculate the clock period as twice the half-period
  constant cCLK_PER  : time := gCLK_HPER * 2;

  component decoder5_32
    port(i_A  : in std_logic_vector(4 downto 0);
         o_B  : out std_logic_vector(31 downto 0));
  end component;
  -- Temporary signals to connect to the dff component.
  signal s_CLK : std_logic;
  signal s_In : std_logic_vector(4 downto 0);
  signal s_Out : std_logic_vector(31 downto 0);

begin

  dec: decoder5_32
  port map(s_In,s_Out);

  -- This process sets the clock value (low for gCLK_HPER, then high
  -- for gCLK_HPER). Absent a "wait" command, processes restart 
  -- at the beginning once they have reached the final statement.
  P_CLK: process
  begin
    s_CLK <= '0';
    wait for gCLK_HPER;
    s_CLK <= '1';
    wait for gCLK_HPER;
  end process;
  
  -- Testbench process  
  P_TB: process
  begin

    s_In <= "00000";
    wait for cCLK_PER;

    s_In <= "00001";
    wait for cCLK_PER;  

    s_In <= "00010";
    wait for cCLK_PER;  

    s_In <= "00011";
    wait for cCLK_PER;  

    s_In <= "11111";
    wait for cCLK_PER;  

    wait;
  end process;
  
end behavior;