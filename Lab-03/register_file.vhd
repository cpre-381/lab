-------------------------------------------------------------------------
-- Joseph Zambreno
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- dff.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of an edge-triggered
-- flip-flop with parallel access and reset.
--
--
-- NOTES:
-- 9/07/08 by JAZ::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.register_array_type.all;

entity Register_File is
  generic(N: integer := 32);
  port(i_CLK	    : in std_logic;
       i_RST	    : in std_logic;
       rt	    : in std_logic_vector(4 downto 0);
       rd	    : in std_logic_vector(4 downto 0);
       rw	    : in std_logic_vector(4 downto 0);
       WE           : in std_logic;
       Data         : in std_logic_vector(N-1 downto 0);
       o_rt         : out std_logic_vector(N-1 downto 0);
       o_rd         : out std_logic_vector(N-1 downto 0));
end Register_File;

architecture structural of Register_File is
  signal s_rw : std_logic_vector(31 downto 0);
  signal WEandRW : std_logic_vector(31 downto 0);
  signal o_Q : registerArray;
  component dff
    generic(N: integer := 32);
    port(i_CLK        : in std_logic;                          -- Clock input
         i_RST        : in std_logic;                          -- Reset input
         i_WE         : in std_logic;                          -- Write enable input
         i_D          : in std_logic_vector(N-1 downto 0);     -- Data value input
         o_Q          : out std_logic_vector(N-1 downto 0));   -- Data value output
  end component;

  component decoder5_32
    port(i_A  : in std_logic_vector(4 downto 0);
         o_B  : out std_logic_vector(31 downto 0));
  end component;

  component mux32_1
    port(i_A  : in registerArray;
         i_S  : in std_logic_vector(4 downto 0);
         o_F  : out std_logic_vector(31 downto 0));
  end component;
  begin
  
  -- Create a multiplexed input to the FF based on i_WE
  write_decoder: decoder5_32
    port map(rw, s_rw);
  read_mux_t: mux32_1
    port map(o_Q, rt, o_rt);
  read_mux_d: mux32_1
    port map(o_Q, rd, o_rd);
  reg_0: dff
    port map(i_CLK => i_CLK,
	 i_RST => i_RST,
         i_WE => '0',
         i_D => Data,
         o_Q => o_Q(0));
  G1: for i in 1 to 31 generate
    WEandRW(i) <= WE and s_rw(i);
    reg_i: dff
    port map(i_CLK => i_CLK,
         i_RST => i_RST,
         i_WE => WEandRW(i),
         i_D => Data,
         o_Q => o_Q(i));   
  end generate;
end structural;
