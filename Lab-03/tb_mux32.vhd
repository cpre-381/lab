-------------------------------------------------------------------------
-- Ryan Wade
-- CPRE 381
-- Iowa State University
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.register_array_type.all;

entity tb_mux32 is
  generic(gCLK_HPER   : time := 50 ns);
end tb_mux32;

architecture behavior of tb_mux32 is
  
  -- Calculate the clock period as twice the half-period
  constant cCLK_PER  : time := gCLK_HPER * 2;

  component mux32_1
    port(i_A  : in registerArray;
         i_S  : in std_logic_vector(4 downto 0);
         o_F  : out std_logic_vector(31 downto 0));
  end component;
  -- Temporary signals to connect to the dff component.
  signal s_A : registerArray := (x"00000000",x"FFFFFFFF",x"FEFEFEFE",x"FDFDFDFD",x"FCFCFCFC",x"FBFBFBFB",x"FAFAFAFA",x"F9F9F9F9",x"F8F8F8F8",x"F7F7F7F7",x"F6F6F6F6",x"F5F5F5F5",x"F4F4F4F4",x"F3F3F3F3",x"F2F2F2F2",x"F1F1F1F1",x"F0F0F0F0",x"E0E0E0E0",x"D0D0D0D0",x"C0C0C0C0",x"B0B0B0B0",x"A0A0A0A0",x"90909090",x"80808080",x"70707070",x"60606060",x"50505050",x"40404040",x"30303030",x"20202020",x"10101010",x"AAAAAAAA");
  signal s_CLK : std_logic;
  signal s_In : std_logic_vector(4 downto 0);
  signal s_Out : std_logic_vector(31 downto 0);

begin

  mux: mux32_1
  port map(S_A,s_In,s_Out);

  -- This process sets the clock value (low for gCLK_HPER, then high
  -- for gCLK_HPER). Absent a "wait" command, processes restart 
  -- at the beginning once they have reached the final statement.
  P_CLK: process
  begin
    s_CLK <= '0';
    wait for gCLK_HPER;
    s_CLK <= '1';
    wait for gCLK_HPER;
  end process;
  
  -- Testbench process  
  P_TB: process
  begin

    s_In <= "00000";
    wait for cCLK_PER;

    s_In <= "00001";
    wait for cCLK_PER;  

    s_In <= "00010";
    wait for cCLK_PER;  

    s_In <= "00011";
    wait for cCLK_PER;  

    s_In <= "11111";
    wait for cCLK_PER;  

    wait;
  end process;
  
end behavior;