library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Decoder5_32 is
  
  port(i_A  : in std_logic_vector(4 downto 0);
       o_B  : out std_logic_vector(31 downto 0));
end Decoder5_32;


architecture dataflow of Decoder5_32 is
  signal t_out : unsigned(31 downto 0) := x"00000001";
  begin
    o_B <= std_logic_vector(SHIFT_LEFT(unsigned(t_out), to_integer(unsigned(i_A))));
end dataflow;