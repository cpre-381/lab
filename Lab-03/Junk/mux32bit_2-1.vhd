------------------------------------------------------------------------
-- Ryan Wade
-- CPRE 381
-- Iowa State University
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity mux2_n_dataflow is
  generic(N : integer := 32);
  port(i_A  : in std_logic_vector(N-1 downto 0);
       i_B  : in std_logic_vector(N-1 downto 0);
       i_S  : in std_logic;
       o_F  : out std_logic_vector(N-1 downto 0));

end mux2_n_dataflow;

architecture dataflow of mux2_n_dataflow is

  -- Signals to store A*x, B*x
  signal ts     : std_logic_vector(N-1 downto 0);
  signal tNotS : std_logic_vector(N-1 downto 0);
  signal tAndA : std_logic_vector(N-1 downto 0);
  signal tAndB : std_logic_vector(N-1 downto 0);

begin

  G1: for i in 0 to N-1 generate
    tS(i) <= i_S;
  end generate;
  tNotS  <=       not tS;
  tAndA  <= i_A   and tNotS;
  tAndB  <= i_B   and ts;
  o_F    <= tAndA or  tAndB;
  
end dataflow;