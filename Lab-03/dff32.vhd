-------------------------------------------------------------------------
-- Joseph Zambreno
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- dff.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of an edge-triggered
-- flip-flop with parallel access and reset.
--
--
-- NOTES:
-- 9/07/08 by JAZ::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.register_array_type.all;

entity dff_32 is
  generic(N: integer := 32);
  port(i_CLK        : in std_logic;                          -- Clock input
       i_RST        : in std_logic;                          -- Reset input
       i_WE         : in std_logic_vector(N-1 downto 0);     -- Write enable input
       i_D          : in std_logic_vector(N-1 downto 0);     -- Data value input
       o_Q          : out registerArray);                    -- Data value output
end dff_32;

architecture structural of dff_32 is

  component dff
    generic(N: integer := 32);
    port(i_CLK        : in std_logic;                          -- Clock input
         i_RST        : in std_logic;                          -- Reset input
         i_WE         : in std_logic;                          -- Write enable input
         i_D          : in std_logic_vector(N-1 downto 0);     -- Data value input
         o_Q          : out std_logic_vector(N-1 downto 0));   -- Data value output
  end component;
  begin
  
  -- Create a multiplexed input to the FF based on i_WE
  G1: for i in 0 to 31 generate
    reg_i: dff
    port map(i_CLK => i_CLK,
         i_RST => i_RST,
         i_WE => i_WE(i),
         i_D => i_D,
         o_Q => o_Q(i));   
  end generate;
end structural;
