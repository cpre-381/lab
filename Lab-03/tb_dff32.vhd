-------------------------------------------------------------------------
-- Joseph Zambreno
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- tb_dff.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains a simple VHDL testbench for the
-- edge-triggered flip-flop with parallel access and reset.
--
--
-- NOTES:
-- 9/07/08 by JAZ::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.register_array_type.all;

entity tb_dff32 is
  generic(gCLK_HPER   : time := 50 ns);
end tb_dff32;

architecture behavior of tb_dff32 is
  
  -- Calculate the clock period as twice the half-period
  constant cCLK_PER  : time := gCLK_HPER * 2;


  component dff_32
    generic(N: integer := 32);
    port(i_CLK        : in std_logic;                          -- Clock input
         i_RST        : in std_logic;                          -- Reset input
         i_WE         : in std_logic_vector(N-1 downto 0);     -- Write enable input
         i_D          : in std_logic_vector(N-1 downto 0);     -- Data value input
         o_Q          : out registerArray);                    -- Data value output
  end component;
  -- Temporary signals to connect to the dff component.
  signal s_CLK, s_RST : std_logic;
  signal s_Q : registerArray;
  signal s_D, s_WE : std_logic_vector(32-1 downto 0);

begin

  RF: dff_32
  port map(i_CLK => s_CLK, 
           i_RST => s_RST,
           i_WE  => s_WE,
           i_D   => s_D,
           o_Q   => s_Q);

  -- This process sets the clock value (low for gCLK_HPER, then high
  -- for gCLK_HPER). Absent a "wait" command, processes restart 
  -- at the beginning once they have reached the final statement.
  P_CLK: process
  begin
    s_CLK <= '0';
    wait for gCLK_HPER;
    s_CLK <= '1';
    wait for gCLK_HPER;
  end process;
  
  -- Testbench process  
  P_TB: process
  begin
    -- Reset the FF
    s_RST <= '1';
    s_WE  <= x"00000000";
    s_D   <= x"0F00000F";
    wait for cCLK_PER;

    -- Store '1'
    s_RST <= '0';
    s_WE  <= x"00000001";
    s_D   <= x"0F00000F";
    wait for cCLK_PER;  

    -- Keep '1'
    s_RST <= '0';
    s_WE  <= x"00000010";
    s_D   <= x"F000F000";
    wait for cCLK_PER;  

    -- Store '0'    
    s_RST <= '0';
    s_WE  <= x"10000000";
    s_D   <= x"F000F000";
    wait for cCLK_PER;  

    -- Keep '0'
    s_RST <= '0';
    s_WE  <= x"01000000";
    s_D   <= x"F000F000";
    wait for cCLK_PER;  

    wait;
  end process;
  
end behavior;