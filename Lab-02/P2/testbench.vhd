------------------------------------------------------------------------
-- Ryan Wade
-- CPRE 381
-- Iowa State University
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity mux_testbench is
  generic(S : integer := 8);
  port(o_Dataflow   : out std_logic_vector(S-1 downto 0);
       o_structure  : out std_logic_vector(S-1 downto 0));

end mux_testbench;

architecture structure of mux_testbench is

  component mux2_n_dataflow
    generic(N : integer := 14);
    port(i_A  : in std_logic_vector(N-1 downto 0);
         i_B  : in std_logic_vector(N-1 downto 0);
         i_S  : in std_logic;
         o_F  : out std_logic_vector(N-1 downto 0));
  end component;

  component mux2_n
    generic(N : integer := 14);
    port(i_A  : in std_logic_vector(N-1 downto 0);
         i_B  : in std_logic_vector(N-1 downto 0);
         i_S  : in std_logic;
         o_F  : out std_logic_vector(N-1 downto 0));
  end component;
  
  signal i_tA : std_logic_vector(S-1 downto 0);
  signal i_tB : std_logic_vector(S-1 downto 0);
  signal i_tS : std_logic;

begin


  g_mux: mux2_n
    generic map(N => 8)
    port map(i_A  => i_tA,
             i_B  => i_tB,
             i_S  => i_tS,
  	          o_F  => o_Structure);
  g_mux_dataflow: mux2_n_dataflow
    generic map(N => 8)
    port map(i_A  => i_tA,
             i_B  => i_tB,
             i_S  => i_tS,
  	          o_F  => o_dataflow);
  process
    begin
      I_tA <= x"FF";
      I_tB <= x"00";
      I_tS <= '1';
      wait for 100 ps;
      
      I_tS <= '0';
      wait for 100 ps;

      I_tA <= x"00";
      I_tB <= x"FF";      
      I_tS <= '1';
      wait for 100 ps;      

      I_tS <= '0';
      wait for 100 ps;
      
  end process;
  
end structure;