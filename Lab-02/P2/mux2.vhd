------------------------------------------------------------------------
-- Ryan Wade
-- CPRE 381
-- Iowa State University
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity mux2 is
  port(i_A  : in std_logic;
       i_B  : in std_logic;
       i_S  : in std_logic;
       o_F  : out std_logic);

end mux2;

architecture structure of mux2 is

  component and2
    port(i_A  : in std_logic;
         i_B  : in std_logic;
         o_F  : out std_logic);
  end component;

  component or2
    port(i_A  : in std_logic;
         i_B  : in std_logic;
         o_F  : out std_logic);
  end component;

  component inv
    port(i_A  : in std_logic;
         o_F  : out std_logic);
  end component;

  -- Signals to store A*x, B*x
  signal tNotS : std_logic;
  signal tAndA : std_logic;
  signal tAndB : std_logic;

begin


  inv_1: inv
    port MAP(i_A => i_S,
           o_F => tNotS);
  and_1: and2
    port MAP(i_A => tNotS,
             i_B => i_A,
             o_F => tAndA);
  and_2: and2
    port MAP(i_A => i_S,
             i_B => i_B,
             o_F => tAndB);
  or_1: or2
    port MAP(i_A => tAndA,
             i_B => tAndB,
             o_F => o_F);
  
end structure;