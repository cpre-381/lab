------------------------------------------------------------------------
-- Joseph Zambreno
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- generate_example.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an example of using generic ports to
-- drive a "generate / for" block. 
--
--
-- NOTES:
-- 8/27/09 by JAZ::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity mux2_n is
  generic(N : integer := 14);
  port(i_A  : in std_logic_vector(N-1 downto 0);
       i_B  : in std_logic_vector(N-1 downto 0);
       i_S  : in std_logic;
       o_F  : out std_logic_vector(N-1 downto 0));

end mux2_n;

architecture structure of mux2_n is

  component and2
    port(i_A  : in std_logic;
         i_B  : in std_logic;
         o_F  : out std_logic);
  end component;

  component or2
    port(i_A  : in std_logic;
         i_B  : in std_logic;
         o_F  : out std_logic);
  end component;

  component inv
    port(i_A  : in std_logic;
         o_F  : out std_logic);
  end component;

  -- Signals to store A*x, B*x
  signal tNotS : std_logic_vector(N-1 downto 0);
  signal tAndA : std_logic_vector(N-1 downto 0);
  signal tAndB : std_logic_vector(N-1 downto 0);

begin

  G1: for i in 0 to N-1 generate
    inv_i1: inv
      port MAP(i_S      , tNotS(i)          );
    and_i1: and2
      port MAP(tNotS(i) , i_A(i)  ,tAndA(i) );
    and_i2: and2
      port MAP(i_S      , i_B(i)  , tAndB(i));
    or_i1: or2
      port MAP(tAndA(i) , tAndB(i), o_F(i)  );
  end generate;
  
end structure;