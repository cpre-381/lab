library IEEE;
use IEEE.std_logic_1164.all;
use work.FullAdderStructural;
entity AdderSubtracter is
  Generic (S: integer := 8);
  
  port(i_Operand1 : in std_logic_vector(S-1 downto 0);
       i_Operand2 : in std_logic_vector(S-1 downto 0);
       i_Carryin  : in std_logic;
       i_Select   : in std_logic;
       o_Carryout : out std_logic;
       o_Sum      : out std_logic_vector(S-1 downto 0));
       
end AdderSubtracter;
  
architecture structure of AdderSubtracter is
  
  component FullAdderStructural is
    generic(N: integer := S);
  
    port(i_A : in std_logic_vector(N-1 downto 0);
         i_B : in std_logic_vector(N-1 downto 0);
         i_C : in std_logic;
         o_S : out std_logic_vector(N-1 downto 0);
         o_C : out std_logic);
         
  end component;
    
    
  component ones_compliment is
    generic(N : integer := S);
    
    port(i_A  : in std_logic_vector(N-1 downto 0);
         o_F  : out std_logic_vector(N-1 downto 0));
  end component;
  
  component mux2_n_dataflow is
    generic(N : integer := S);
    port(i_A  : in std_logic_vector(N-1 downto 0);
    i_B  : in std_logic_vector(N-1 downto 0);
    i_S  : in std_logic;
    o_F  : out std_logic_vector(N-1 downto 0));
  end component;
  
  signal Operand2_compliment : std_logic_vector(S-1 downto 0);
  signal operand2_selection : std_logic_vector(S-1 downto 0);
  begin
    
    g_onescompliment : ones_compliment
      port map(i_A => i_Operand2,
               o_F => Operand2_compliment); 
    g_mux : mux2_n_dataflow
      port map(i_A => i_Operand2,
               i_B => Operand2_compliment,
               i_S => i_Select,
               o_F => operand2_selection);
    g_FullAdder : FullAdderStructural
      port map(i_A => i_Operand1,
               i_B => operand2_selection,
               i_C => i_Select,
               o_S => o_Sum,
               o_C => o_Carryout);
  
end structure;