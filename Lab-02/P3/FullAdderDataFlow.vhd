library IEEE;
use IEEE.std_logic_1164.all;

entity FullAdderDataFlow is
  
  generic(N: integer := 8);
  
  port(i_A : in std_logic_vector(N-1 downto 0);
       i_B : in std_logic_vector(N-1 downto 0);
       i_C : in std_logic;
       o_S : out std_logic_vector(N-1 downto 0);
       o_C : out std_logic);

end FullAdderDataFlow;

architecture dataflow of FullAdderDataFlow is
  
  signal tC    : std_logic_vector(N downto 0);
  signal AXorB : std_logic_vector(N-1 downto 0);
  signal AAndB : std_logic_vector(N-1 downto 0);
  signal tAndC : std_logic_vector(N-1 downto 0);
  
  begin
    tC(0) <= i_C;
    AXorB <= i_A xor i_B;
    AAndB <= i_A and i_B;
    tAndC <= AXorB and tC;
    G1: for i in 0 to N-1 generate      
      tC(i+1) <= AAndB(i) or tAndC(i);
    end generate;
    o_S   <= AXorB xor tC;
        
    o_C <= tC(N);
      
    
  end dataflow;