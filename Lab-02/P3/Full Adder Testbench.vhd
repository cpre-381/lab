------------------------------------------------------------------------
-- Ryan Wade
-- CPRE 381
-- Iowa State University
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity FullAdder_testbench is
  generic(S : integer := 8);
  port(o_Dataflow   : out std_logic_vector(S-1 downto 0);
       c_Dataflow   : out std_logic;
       o_structure  : out std_logic_vector(S-1 downto 0);
       c_structure  : out std_logic);

end FullAdder_testbench;

architecture structure of FullAdder_testbench is

  component FullAdderStructural is
    generic(N : integer := 8);
    port(i_A : in std_logic_vector(N-1 downto 0);
         i_B : in std_logic_vector(N-1 downto 0);
         i_C : in std_logic;
         o_S : out std_logic_vector(N-1 downto 0);
         o_C : out std_logic);
  end component;

  component FullAdderDataFlow
    generic(N : integer := 8);
    port(i_A : in std_logic_vector(N-1 downto 0);
         i_B : in std_logic_vector(N-1 downto 0);
         i_C : in std_logic;
         o_S : out std_logic_vector(N-1 downto 0);
         o_C : out std_logic);
  end component;
  
  signal i_tA : std_logic_vector(S-1 downto 0);
  signal i_tB : std_logic_vector(S-1 downto 0);
  signal i_tC : std_logic;

begin


  g_structure: FullAdderStructural
    generic map(N => 8)
    port map(i_A  => i_tA,
             i_B  => i_tB,
             i_C  => i_tC,
             o_S  => o_Structure,
	     o_C  => c_Structure);
  g_dataflow: FullAdderDataFlow
    generic map(N => 8)
    port map(i_A  => i_tA,
             i_B  => i_tB,
             i_C  => i_tC,
  	     o_S  => o_dataflow,
  	     o_C  => c_dataflow);
  process
    begin
      I_tA <= x"00";
      I_tB <= x"01";
      I_tC <= '0';
      wait for 100 ps;
      
      I_tC <= '1';
      wait for 100 ps;

      I_tA <= x"01";
      I_tB <= x"01";      
      I_tC <= '1';
      wait for 100 ps;

      I_tA <= x"FF";
      I_tB <= x"01";      
      I_tC <= '0';
      wait for 100 ps;

      I_tC <= '1';
      wait for 100 ps;

      I_tA <= x"AA";
      I_tB <= x"A0";      
      I_tC <= '0';
      wait for 100 ps;
      
  end process;
  
end structure;