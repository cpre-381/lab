library IEEE;
use IEEE.std_logic_1164.all;

entity testbench1 is
  generic(S: integer := 32);
  
  port(o_S : out std_logic_vector(S-1 downto 0);
       o_D : out std_logic_vector(S-1 downto 0));
      
end testbench1;


architecture behavioral of testbench1 is
component Ones_Compliment
  generic(N : integer := 14);
      port(i_A  : in std_logic_vector(N-1 downto 0);
       o_F  : out std_logic_vector(N-1 downto 0));
    end component;
    
  component ones_compliment_dataflow
    generic(N : integer := 32);
      port(i_A  : in std_logic_vector(N-1 downto 0);
       o_F  : out std_logic_vector(N-1 downto 0));
  end component;
     
  signal i_T : std_logic_vector(S-1 downto 0);  
     
begin
  
 g_OnesCompliment : Ones_Compliment
      generic map(N => S)
      port map(i_A => i_T,
               o_F => o_S);
    
    
 g_Uones_compliment_dataflow : ones_compliment_dataflow
      generic map(N => S)
      port map(i_A => i_T,
               o_F => o_D);  

process
  begin
    i_T <= "00001111000011110000111100001111";
    wait for 100 ns;
    
    i_T <= "11001100110011001100110011001100";
    wait for 100 ns;
    
    i_T <= "01010101010101010101010101010101";
    wait for 100 ns;
                         
    
end process;
       
end behavioral;